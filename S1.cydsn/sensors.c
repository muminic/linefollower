/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdlib.h>
#include "stdio.h"
#include "sensors.h"
#include "console.h"
#include "timer_time.h"
#include "bt_app.h"
#include "log2.h"


#define DETECT_VALUE 400
int lastValue = 0;

void UART_BT_PutInt(int number){
    char text[3];
    itoa(number, text, 10); 
    UART_BT_PutString(text);
}


void readSensors()
{
    
    for (int i = 0; i <= 15; i++)
    {
        gSensors.readValue[i] = (ADC_SAR_GetResult16(i));
    }
}

int checkSensors(int time, int count)
{
    int on_line = 0;
    int sensorDetected = 0;
    int sensorReadSum[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    //readSensors();
    
    int delayTime = time / count ;
    
    for(int j = 0; j <= count-1; j++)
    {
        readSensors();
        for (int i = 0; i <= 15; i++)
        {    
            gSensors.readValuebuffer[j][i] = gSensors.readValue[i];     
        }
        CyDelayUs(delayTime);
        /*log_debug("SensorsRAW: %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d ", gSensors.readValue[15],gSensors.readValue[14],gSensors.readValue[13],
    gSensors.readValue[12],gSensors.readValue[11],gSensors.readValue[10],gSensors.readValue[9],gSensors.readValue[8],gSensors.readValue[7],
    gSensors.readValue[6],gSensors.readValue[5],gSensors.readValue[4],gSensors.readValue[3],gSensors.readValue[2],gSensors.readValue[1],
    gSensors.readValue[0]);
    
        */
    }
    for(int i = 0; i <= 15; i++)
    {
        for (int j = 0; j <= count-1; j++)
        {    
            sensorReadSum[i] += gSensors.readValuebuffer[j][i];   
        }
    }
    
    for (int i = 0; i <= 15; i++)
    {    
        gSensors.calculatedValue[i] = sensorReadSum[i] / count; 
        
    }
    
    log_debug("SensorsRAWCalc: %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d ", gSensors.calculatedValue[15],gSensors.calculatedValue[14],gSensors.calculatedValue[13],
    gSensors.calculatedValue[12],gSensors.calculatedValue[11],gSensors.calculatedValue[10],gSensors.calculatedValue[9],gSensors.calculatedValue[8],gSensors.calculatedValue[7],
    gSensors.calculatedValue[6],gSensors.calculatedValue[5],gSensors.calculatedValue[4],gSensors.calculatedValue[3],gSensors.calculatedValue[2],gSensors.calculatedValue[1],
    gSensors.calculatedValue[0]);
    
    
    
    
    
    
    for (int i = 0; i <= 15; i++)
    {      
        int check = gSensors.calculatedValue[i];
        
        if (check >= DETECT_VALUE)
        {
            gSensors.detectedSensors[i] = 1;
            on_line = 1;
            sensorDetected += 1;
        }
        else
        {
            gSensors.detectedSensors[i] = 0;
        }
           
    }
    
    log_debug("Sensors: %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d ", gSensors.detectedSensors[15],gSensors.detectedSensors[14],gSensors.detectedSensors[13],
    gSensors.detectedSensors[12],gSensors.detectedSensors[11],gSensors.detectedSensors[10],gSensors.detectedSensors[9],gSensors.detectedSensors[8],gSensors.detectedSensors[7],
    gSensors.detectedSensors[6],gSensors.detectedSensors[5],gSensors.detectedSensors[4],gSensors.detectedSensors[3],gSensors.detectedSensors[2],gSensors.detectedSensors[1],
    gSensors.detectedSensors[0]);
    
    if (on_line)
    {
        if (sensorDetected == 1 && gSensors.detectedSensors[0] == 0 && gSensors.detectedSensors[15] == 0)
        {
            log_info("NO LINE");
            return lastValue;
            log_error("NO LINE NOT WORKING");
        }
    }
    
    
    //if (sensorDetected >= 14) { btComands.driving = 0;}
    
    
    
    if(!on_line)
    {
        // If it last read to the left of center, return 0.
        if(lastValue < (16-1)*1000/2)  //(16-1) *1000/2 = 15000/2 = 7500
        // if (lastValue <= 1000)
            return 0;

        // If it last read to the right of center, return the max.
        else
        //else if (lastValue >= 14000)
            return (16-1)*1000;
        //else
            //return lastValue;

    }
    
    int32_t dalamais = 0;
    int32_t dalitajs = 0;
    
    for (int i= 0; i < 16; i++)
    {
        dalamais += i*1000*gSensors.detectedSensors[i];    
    }
    
    for (int i= 0; i < 16; i++)
    {
        dalitajs += gSensors.detectedSensors[i];    
    }
    lastValue = (int)(dalamais/dalitajs);
    return lastValue;
    
}

int cmpfunc (const void * a, const void * b) 
        {
            return ( *(int*)a - *(int*)b );
        }

void calibrateSensors(int measures, int average)
{
    int32_t senValue [16][measures+1];
    for (int y = 1; y <= measures; y++)
    {
        for (int i = 0; i <= 16; i++)
        {
            senValue [i][y] = (ADC_SAR_GetResult16(i));
            console_print_int(senValue [i][y]);
            console_print ("  ");
            
        }
        console_print("\r\n");
        CyDelay(250);
        
    }
    console_print("\r\n");
    
    int32_t senSum [15] ;
    
    for (int i = 0; i <= 15; i++)
        {
            senSum[i] = 0;
        }

    for (int i = 0; i <= 15; i++)
    {
        qsort(senValue[i],measures,sizeof(int),cmpfunc);
    }
        
    console_print("\r\n");
    for (int y = 0; y <= 15; y++)
    {
        for (int i = 1; i <= average; i++)
        {
            senSum [y] = senSum[y] + senValue[y][i];     
            console_print_int(senValue[y][i]);
            console_print (" n ");       
        }
        
        for (int i = measures - average; i <= measures-1; i++)
        {
            senSum [y] = senSum[y] + senValue[y][i];     
            console_print_int(senValue[y][i]);
            console_print (" n ");       
        }
        
        gSensors.thresholdValue[y] = 0;
        gSensors.thresholdValue[y] = senSum[y] / (average * 2);
        console_print_int(gSensors.thresholdValue[y]);
        console_print("\r\n");
    }
}







void sensorsRaw()
{
        
        UART_BT_PutString("    ########  Sensors ######## ");  
        UART_BT_PutString("\r\n");

        UART_BT_PutString("\r\n");
        UART_BT_PutInt(ADC_SAR_GetResult16(0));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(1));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(2));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(3));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(4));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(5));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(6));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(7));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(8));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(9));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(10));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(11));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(12));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(13));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(14));
        
        UART_BT_PutString(" ");
        UART_BT_PutInt(ADC_SAR_GetResult16(15));
        
        
        UART_BT_PutString("\r\n");  
        CyDelay(500);

}

void sensorsRawConsole()
{
    
    console_print("\033[2J");   // Erase the screen with the background colour and moves the cursor to home.
    console_print("\033[r");

    console_printChr('\f');
    console_print("\r\n");
    console_print("#####################  Sensors ##################### ");  
    console_print("\r\n");
    console_print("\r\n");

    for (int i = 0; i <= 15; i++)
    {
        console_print_int(ADC_SAR_GetResult16(i));
        console_print ("  ");
    }
    
    console_print("\r\n");  
    CyDelay(500);


}

void consolecheckSensors(void)
{
    checkSensors(1,1);
    
    //console_print("\033[2J");   // Erase the screen with the background colour and moves the cursor to home.
    //console_print("\033[r");

    //console_printChr('\f');
    console_print("\r\n");
    console_print("#####################  Sensors ##################### ");  
    console_print("\r\n");
    console_print("\r\n");

    for (int i = 0; i <= 15; i++)
    {
        console_print_int(gSensors.detectedSensors[i]);
        console_print ("  ");
    }
    
    console_print("\r\n");  
    CyDelay(500);
    
}
/* [] END OF FILE */
