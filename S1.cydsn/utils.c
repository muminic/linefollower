/* ========================================
 * Copyright (c) RobotuSkola, 2018-2019
 * Written by
 * - Maris Becs <maris.becs@gmail.com>, 2016-2019
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute
 * this software is freely granted, provided that
 * this notice is preserved.
 * ========================================
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "utils.h"
#include "log2.h"

//=============================================================================
// Public function definitions
//=============================================================================
const char* double2str(double number)
{
    static char strBuf[32+1] = {0};;
    memset(strBuf, 0, sizeof(strBuf));

    const int fraction = (int)(number * 100) - (int)number * 100;
    snprintf(strBuf, sizeof(strBuf), "%d.%02d", (int)number, abs(fraction));
    strBuf[32] = '\0';

    return strBuf;
}
const char* float2str(float number)
{
    static char strBuf[32+1] = {0};;
    memset(strBuf, 0, sizeof(strBuf));

    const int fraction = (int)(number * 100) - (int)number * 100;
    snprintf(strBuf, sizeof(strBuf), "%d.%02d", (int)number, abs(fraction));
    strBuf[32] = '\0';

    return strBuf;
}

const char* byte2str(uint8_t number)
{
    static char strBuf[8+1] = {0};
    memset(strBuf, 0, sizeof(strBuf));

    const uint8_t mask = 0x80;  // 1000 0000
    for (int i = 0; i < 8; i++) {
        if (((mask >> i) & number) == (mask >> i)) {
            strBuf[i] = '1';
        }
        else {
            strBuf[i] = '0';
        }
    }

    return strBuf;
}

/* [] END OF FILE */
