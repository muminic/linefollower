/* ========================================
 * Copyright (c) RobotuSkola, 2018-2019
 * Written by
 * - Maris Becs <maris.becs@gmail.com>, 2016-2019
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute
 * this software is freely granted, provided that
 * this notice is preserved.
 * ========================================
*/
#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

//=============================================================================
// Public defines and typedefs
//=============================================================================
typedef struct {
    int x;
    int y;
    int z;
} vect3d;

// \brief Convenience macro for counting elements in arrays
#define ARRAY_SIZE(x)                       (sizeof(x) / sizeof((x)[0]))

#define Rd_bits(value, mask)                ((value) & (mask))
#define Clr_bits(value, mask)               ((value) &= ~(mask))
#define Set_bits(value, mask)               ((value) |= (mask))

#define bitRead(value, bit)                 (((value) >> (bit)) & 0x01)
#define bitSet(value, bit)                  ((value) |= (1UL << (bit)))
#define bitClear(value, bit)                ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue)      (bitvalue ? bitSet(value, bit) : bitClear(value, bit))

//=============================================================================
// Public function declarations
//=============================================================================
const char* double2str(double number);
const char* float2str(float number);
const char* byte2str(uint8_t number);

#endif /* UTILS_H */
