/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "drive.h"
#include "project.h"
#include "sensors.h"

#include "stdlib.h"

#define TICKS_PER_REVOLUTION 5
#define MAX_PWM     255
#define MIN_PWM     5
#define CM_TO_TICKS_SCALE  2 //1.5  // need to calibrate
#define ANGLE_TO_TICKS_SCALE 0.06 // need to calibrate

void Motor_Init(void){
    MOT_PWM_Start();
    MOT_CTRL_reg_Write(0); 
}

void drive_backward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x9); 
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    //gMotor.max_pwm_Limited_L = 5;
    //gMotor.max_pwm_Limited_R = 5;
}


void drive_forward(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x6); //A
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    
}

void turn_left(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0x5); 
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    //gMotor.max_pwm_Limited_L = 5;
    //gMotor.max_pwm_Limited_R = 5;
}

void turn_right(int left_pwm, int right_pwm){
    MOT_CTRL_reg_Write(0xA); 
    MOT_PWM_WriteCompare1(left_pwm);
    MOT_PWM_WriteCompare2(right_pwm);
    //gMotor.max_pwm_Limited_L = 5;
    //gMotor.max_pwm_Limited_R = 5;
}

void brake(void){
    MOT_CTRL_reg_Write(0xF); 
    MOT_PWM_WriteCompare1(0);
    MOT_PWM_WriteCompare2(0);
    //gMotor.max_pwm_Limited_L = 5;
    //gMotor.max_pwm_Limited_R = 5;
    CyDelay(10);
}

void drive_motors (int leftSpeed, int rightSpeed)
{
    if (leftSpeed > MAX_PWM) rightSpeed = rightSpeed - (leftSpeed - MAX_PWM);
    if (leftSpeed < -MAX_PWM) rightSpeed = rightSpeed + abs (( MAX_PWM + leftSpeed));
    
    if (rightSpeed > MAX_PWM) leftSpeed = leftSpeed - (rightSpeed - MAX_PWM);
    if (rightSpeed < -MAX_PWM) leftSpeed = leftSpeed + abs (( MAX_PWM + rightSpeed));
    
    if (leftSpeed >= MAX_PWM) leftSpeed = MAX_PWM;
    if (leftSpeed <= -MAX_PWM) leftSpeed = -MAX_PWM;
    if (rightSpeed >= MAX_PWM) rightSpeed = MAX_PWM;
    if (rightSpeed <= -MAX_PWM) rightSpeed = -MAX_PWM;
    
    if (leftSpeed >= 0 && rightSpeed >= 0)
    {
        drive_forward(leftSpeed, rightSpeed);        
    }
    else if (leftSpeed < 0 && rightSpeed < 0)
    {
        drive_backward(leftSpeed,rightSpeed);    
    }
    else if (leftSpeed <= 0 && rightSpeed >= 0)
    {
        turn_left(leftSpeed,rightSpeed);    
    }
    else if (leftSpeed > 0 && rightSpeed < 0)
    {
        turn_right(leftSpeed,rightSpeed);    
    }
    else 
    {
        brake();    
    }
}
/* [] END OF FILE */
