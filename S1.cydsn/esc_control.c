/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "esc_control.h"
#include "project.h"
#include "bt_app.h"
#include "log2.h"

#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))


#define SPEED_MIN (1000)                                  // Set the Minimum Speed in microseconds
#define SPEED_MAX (2000) 

#define ESC_CAL_DELAY	(8000)	// Calibration delay (milisecond)
#define ESC_STOP_PULSE	(500)	//

int oMin = 1000; 
int oMax = 2000;
int oESC = 1000;
int oArm = 500;
int escIsSpinning = 0;

void EscArm(void)
{
	Servo_WriteCompare(oArm);
}

void EscStop(void)
{
	Servo_WriteCompare (ESC_STOP_PULSE);
    escIsSpinning = 0;
    btComands.escSpinning = 0;
}
void EscSpeed(int outputESC)
{
	oESC = constrain(outputESC, oMin, oMax);
	Servo_WriteCompare(oESC);
}

void calib(void)
{
	Servo_WriteCompare(oMax);
		CyDelay(ESC_CAL_DELAY);
	Servo_WriteCompare(oMin);
		CyDelay(ESC_CAL_DELAY);
	EscArm();
}
void escSpin()
{

    if (!escIsSpinning)
    {
    log_info("ESC START");
    for (oESC = SPEED_MIN; oESC <=btValue.escSpeed ; oESC += 10) 
    {  // goes from 1000 microseconds to 2000 microseconds
        EscSpeed(oESC);                                    // tell ESC to go to the oESC speed value
        CyDelay(3);                                            // waits 10ms for the ESC to reach speed
    } 
    escIsSpinning = 1;
    log_info("ESC is spinning");
    }
}

/* [] END OF FILE */
