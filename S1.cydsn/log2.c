/* ========================================
 *
 * Copyright RobotuSkola, 2018-2019
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ROBOTU SKOLA.
 *
 * ========================================
*/
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <project.h>
#include "log2.h"
#include "timer_time.h"
#include "utils.h"

/*
#if defined (__GNUC__)
    // NB: Increase the heap size to at least 0x400
    asm(".global _printf_float");
    asm(".global _scanf_float");

    // For GCC compiler revise _write() function for printf functionality
    int _write(int file, char *ptr, int len) 
    {
        file = file;
        for (int i = 0; i < len; i++) {
            DEBUG_UART_PutChar(*ptr++);
        }
        return len;
    }
    */

//=============================================================================
// Private defines and typedefs
//=============================================================================
static const char *level_names[] = {
    "D ",   // DEBUG
    "I ",   // INFO
    "$ ",   // BLUETOOTH
    "$W ",  // WARNING
    "$E "}; // ERROR

//=============================================================================
// Private data definitions
//=============================================================================
static int s_logLevel = 0;

//=============================================================================
// Public function definitions
//=============================================================================
void log_setLevel(int level)
{
    s_logLevel = level;
}

void log_log(int level, const char *fmt, ...)
{
	if (level < s_logLevel) return;

    const psoc_time_t psocTime = getTime();
    char logBuffer[128 + 1] = {0};
	memset(logBuffer, 0, sizeof(logBuffer));
    if (psocTime.sec) {
	    snprintf(logBuffer, sizeof(logBuffer), "%01u.%03u.%02u %s",
                 psocTime.sec, psocTime.ms, psocTime.ms_100th, level_names[level]);
    } else {
        snprintf(logBuffer, sizeof(logBuffer), "%3u.%02u %s",
                 psocTime.ms, psocTime.ms_100th, level_names[level]);
    }

	va_list args;
	va_start(args, fmt);
	vsnprintf(logBuffer + strlen(logBuffer), sizeof(logBuffer) - strlen(logBuffer), fmt, args);
	va_end(args);

    DEBUG_UART_PutString(logBuffer);
    DEBUG_UART_PutString("\r\n");
}
