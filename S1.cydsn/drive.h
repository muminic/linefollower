/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/


#ifndef DRIVE_H
#define DRIVE_H
    
struct gDrivevars
{
    int basic_speed;
    int turn_speed;
    int max_pwm_Limited_L;
    int max_pwm_Limited_R;	
}gMotor;

void drive_motors (int leftSpeed, int rightSpeed);
void Motor_Init(void);
void drive_forward(int left_pwm, int right_pwm);
void drive_backward(int left_pwm, int right_pwm);
void turn_right(int left_pwm, int right_pwm);
void turn_left(int left_pwm, int right_pwm);

#endif /* DRIVE_H */