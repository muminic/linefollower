/*******************************************************************************
* File Name: sen10.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen10_H) /* Pins sen10_H */
#define CY_PINS_sen10_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen10_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen10__PORT == 15 && ((sen10__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen10_Write(uint8 value);
void    sen10_SetDriveMode(uint8 mode);
uint8   sen10_ReadDataReg(void);
uint8   sen10_Read(void);
void    sen10_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen10_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen10_SetDriveMode() function.
     *  @{
     */
        #define sen10_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen10_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen10_DM_RES_UP          PIN_DM_RES_UP
        #define sen10_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen10_DM_OD_LO           PIN_DM_OD_LO
        #define sen10_DM_OD_HI           PIN_DM_OD_HI
        #define sen10_DM_STRONG          PIN_DM_STRONG
        #define sen10_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen10_MASK               sen10__MASK
#define sen10_SHIFT              sen10__SHIFT
#define sen10_WIDTH              1u

/* Interrupt constants */
#if defined(sen10__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen10_SetInterruptMode() function.
     *  @{
     */
        #define sen10_INTR_NONE      (uint16)(0x0000u)
        #define sen10_INTR_RISING    (uint16)(0x0001u)
        #define sen10_INTR_FALLING   (uint16)(0x0002u)
        #define sen10_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen10_INTR_MASK      (0x01u) 
#endif /* (sen10__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen10_PS                     (* (reg8 *) sen10__PS)
/* Data Register */
#define sen10_DR                     (* (reg8 *) sen10__DR)
/* Port Number */
#define sen10_PRT_NUM                (* (reg8 *) sen10__PRT) 
/* Connect to Analog Globals */                                                  
#define sen10_AG                     (* (reg8 *) sen10__AG)                       
/* Analog MUX bux enable */
#define sen10_AMUX                   (* (reg8 *) sen10__AMUX) 
/* Bidirectional Enable */                                                        
#define sen10_BIE                    (* (reg8 *) sen10__BIE)
/* Bit-mask for Aliased Register Access */
#define sen10_BIT_MASK               (* (reg8 *) sen10__BIT_MASK)
/* Bypass Enable */
#define sen10_BYP                    (* (reg8 *) sen10__BYP)
/* Port wide control signals */                                                   
#define sen10_CTL                    (* (reg8 *) sen10__CTL)
/* Drive Modes */
#define sen10_DM0                    (* (reg8 *) sen10__DM0) 
#define sen10_DM1                    (* (reg8 *) sen10__DM1)
#define sen10_DM2                    (* (reg8 *) sen10__DM2) 
/* Input Buffer Disable Override */
#define sen10_INP_DIS                (* (reg8 *) sen10__INP_DIS)
/* LCD Common or Segment Drive */
#define sen10_LCD_COM_SEG            (* (reg8 *) sen10__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen10_LCD_EN                 (* (reg8 *) sen10__LCD_EN)
/* Slew Rate Control */
#define sen10_SLW                    (* (reg8 *) sen10__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen10_PRTDSI__CAPS_SEL       (* (reg8 *) sen10__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen10_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen10__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen10_PRTDSI__OE_SEL0        (* (reg8 *) sen10__PRTDSI__OE_SEL0) 
#define sen10_PRTDSI__OE_SEL1        (* (reg8 *) sen10__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen10_PRTDSI__OUT_SEL0       (* (reg8 *) sen10__PRTDSI__OUT_SEL0) 
#define sen10_PRTDSI__OUT_SEL1       (* (reg8 *) sen10__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen10_PRTDSI__SYNC_OUT       (* (reg8 *) sen10__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen10__SIO_CFG)
    #define sen10_SIO_HYST_EN        (* (reg8 *) sen10__SIO_HYST_EN)
    #define sen10_SIO_REG_HIFREQ     (* (reg8 *) sen10__SIO_REG_HIFREQ)
    #define sen10_SIO_CFG            (* (reg8 *) sen10__SIO_CFG)
    #define sen10_SIO_DIFF           (* (reg8 *) sen10__SIO_DIFF)
#endif /* (sen10__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen10__INTSTAT)
    #define sen10_INTSTAT            (* (reg8 *) sen10__INTSTAT)
    #define sen10_SNAP               (* (reg8 *) sen10__SNAP)
    
	#define sen10_0_INTTYPE_REG 		(* (reg8 *) sen10__0__INTTYPE)
#endif /* (sen10__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen10_H */


/* [] END OF FILE */
