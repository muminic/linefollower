/*******************************************************************************
* File Name: sen15.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen15_H) /* Pins sen15_H */
#define CY_PINS_sen15_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen15_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen15__PORT == 15 && ((sen15__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen15_Write(uint8 value);
void    sen15_SetDriveMode(uint8 mode);
uint8   sen15_ReadDataReg(void);
uint8   sen15_Read(void);
void    sen15_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen15_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen15_SetDriveMode() function.
     *  @{
     */
        #define sen15_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen15_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen15_DM_RES_UP          PIN_DM_RES_UP
        #define sen15_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen15_DM_OD_LO           PIN_DM_OD_LO
        #define sen15_DM_OD_HI           PIN_DM_OD_HI
        #define sen15_DM_STRONG          PIN_DM_STRONG
        #define sen15_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen15_MASK               sen15__MASK
#define sen15_SHIFT              sen15__SHIFT
#define sen15_WIDTH              1u

/* Interrupt constants */
#if defined(sen15__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen15_SetInterruptMode() function.
     *  @{
     */
        #define sen15_INTR_NONE      (uint16)(0x0000u)
        #define sen15_INTR_RISING    (uint16)(0x0001u)
        #define sen15_INTR_FALLING   (uint16)(0x0002u)
        #define sen15_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen15_INTR_MASK      (0x01u) 
#endif /* (sen15__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen15_PS                     (* (reg8 *) sen15__PS)
/* Data Register */
#define sen15_DR                     (* (reg8 *) sen15__DR)
/* Port Number */
#define sen15_PRT_NUM                (* (reg8 *) sen15__PRT) 
/* Connect to Analog Globals */                                                  
#define sen15_AG                     (* (reg8 *) sen15__AG)                       
/* Analog MUX bux enable */
#define sen15_AMUX                   (* (reg8 *) sen15__AMUX) 
/* Bidirectional Enable */                                                        
#define sen15_BIE                    (* (reg8 *) sen15__BIE)
/* Bit-mask for Aliased Register Access */
#define sen15_BIT_MASK               (* (reg8 *) sen15__BIT_MASK)
/* Bypass Enable */
#define sen15_BYP                    (* (reg8 *) sen15__BYP)
/* Port wide control signals */                                                   
#define sen15_CTL                    (* (reg8 *) sen15__CTL)
/* Drive Modes */
#define sen15_DM0                    (* (reg8 *) sen15__DM0) 
#define sen15_DM1                    (* (reg8 *) sen15__DM1)
#define sen15_DM2                    (* (reg8 *) sen15__DM2) 
/* Input Buffer Disable Override */
#define sen15_INP_DIS                (* (reg8 *) sen15__INP_DIS)
/* LCD Common or Segment Drive */
#define sen15_LCD_COM_SEG            (* (reg8 *) sen15__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen15_LCD_EN                 (* (reg8 *) sen15__LCD_EN)
/* Slew Rate Control */
#define sen15_SLW                    (* (reg8 *) sen15__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen15_PRTDSI__CAPS_SEL       (* (reg8 *) sen15__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen15_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen15__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen15_PRTDSI__OE_SEL0        (* (reg8 *) sen15__PRTDSI__OE_SEL0) 
#define sen15_PRTDSI__OE_SEL1        (* (reg8 *) sen15__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen15_PRTDSI__OUT_SEL0       (* (reg8 *) sen15__PRTDSI__OUT_SEL0) 
#define sen15_PRTDSI__OUT_SEL1       (* (reg8 *) sen15__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen15_PRTDSI__SYNC_OUT       (* (reg8 *) sen15__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen15__SIO_CFG)
    #define sen15_SIO_HYST_EN        (* (reg8 *) sen15__SIO_HYST_EN)
    #define sen15_SIO_REG_HIFREQ     (* (reg8 *) sen15__SIO_REG_HIFREQ)
    #define sen15_SIO_CFG            (* (reg8 *) sen15__SIO_CFG)
    #define sen15_SIO_DIFF           (* (reg8 *) sen15__SIO_DIFF)
#endif /* (sen15__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen15__INTSTAT)
    #define sen15_INTSTAT            (* (reg8 *) sen15__INTSTAT)
    #define sen15_SNAP               (* (reg8 *) sen15__SNAP)
    
	#define sen15_0_INTTYPE_REG 		(* (reg8 *) sen15__0__INTTYPE)
#endif /* (sen15__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen15_H */


/* [] END OF FILE */
