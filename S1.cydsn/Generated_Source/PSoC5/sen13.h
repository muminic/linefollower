/*******************************************************************************
* File Name: sen13.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen13_H) /* Pins sen13_H */
#define CY_PINS_sen13_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen13_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen13__PORT == 15 && ((sen13__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen13_Write(uint8 value);
void    sen13_SetDriveMode(uint8 mode);
uint8   sen13_ReadDataReg(void);
uint8   sen13_Read(void);
void    sen13_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen13_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen13_SetDriveMode() function.
     *  @{
     */
        #define sen13_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen13_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen13_DM_RES_UP          PIN_DM_RES_UP
        #define sen13_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen13_DM_OD_LO           PIN_DM_OD_LO
        #define sen13_DM_OD_HI           PIN_DM_OD_HI
        #define sen13_DM_STRONG          PIN_DM_STRONG
        #define sen13_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen13_MASK               sen13__MASK
#define sen13_SHIFT              sen13__SHIFT
#define sen13_WIDTH              1u

/* Interrupt constants */
#if defined(sen13__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen13_SetInterruptMode() function.
     *  @{
     */
        #define sen13_INTR_NONE      (uint16)(0x0000u)
        #define sen13_INTR_RISING    (uint16)(0x0001u)
        #define sen13_INTR_FALLING   (uint16)(0x0002u)
        #define sen13_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen13_INTR_MASK      (0x01u) 
#endif /* (sen13__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen13_PS                     (* (reg8 *) sen13__PS)
/* Data Register */
#define sen13_DR                     (* (reg8 *) sen13__DR)
/* Port Number */
#define sen13_PRT_NUM                (* (reg8 *) sen13__PRT) 
/* Connect to Analog Globals */                                                  
#define sen13_AG                     (* (reg8 *) sen13__AG)                       
/* Analog MUX bux enable */
#define sen13_AMUX                   (* (reg8 *) sen13__AMUX) 
/* Bidirectional Enable */                                                        
#define sen13_BIE                    (* (reg8 *) sen13__BIE)
/* Bit-mask for Aliased Register Access */
#define sen13_BIT_MASK               (* (reg8 *) sen13__BIT_MASK)
/* Bypass Enable */
#define sen13_BYP                    (* (reg8 *) sen13__BYP)
/* Port wide control signals */                                                   
#define sen13_CTL                    (* (reg8 *) sen13__CTL)
/* Drive Modes */
#define sen13_DM0                    (* (reg8 *) sen13__DM0) 
#define sen13_DM1                    (* (reg8 *) sen13__DM1)
#define sen13_DM2                    (* (reg8 *) sen13__DM2) 
/* Input Buffer Disable Override */
#define sen13_INP_DIS                (* (reg8 *) sen13__INP_DIS)
/* LCD Common or Segment Drive */
#define sen13_LCD_COM_SEG            (* (reg8 *) sen13__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen13_LCD_EN                 (* (reg8 *) sen13__LCD_EN)
/* Slew Rate Control */
#define sen13_SLW                    (* (reg8 *) sen13__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen13_PRTDSI__CAPS_SEL       (* (reg8 *) sen13__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen13_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen13__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen13_PRTDSI__OE_SEL0        (* (reg8 *) sen13__PRTDSI__OE_SEL0) 
#define sen13_PRTDSI__OE_SEL1        (* (reg8 *) sen13__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen13_PRTDSI__OUT_SEL0       (* (reg8 *) sen13__PRTDSI__OUT_SEL0) 
#define sen13_PRTDSI__OUT_SEL1       (* (reg8 *) sen13__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen13_PRTDSI__SYNC_OUT       (* (reg8 *) sen13__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen13__SIO_CFG)
    #define sen13_SIO_HYST_EN        (* (reg8 *) sen13__SIO_HYST_EN)
    #define sen13_SIO_REG_HIFREQ     (* (reg8 *) sen13__SIO_REG_HIFREQ)
    #define sen13_SIO_CFG            (* (reg8 *) sen13__SIO_CFG)
    #define sen13_SIO_DIFF           (* (reg8 *) sen13__SIO_DIFF)
#endif /* (sen13__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen13__INTSTAT)
    #define sen13_INTSTAT            (* (reg8 *) sen13__INTSTAT)
    #define sen13_SNAP               (* (reg8 *) sen13__SNAP)
    
	#define sen13_0_INTTYPE_REG 		(* (reg8 *) sen13__0__INTTYPE)
#endif /* (sen13__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen13_H */


/* [] END OF FILE */
