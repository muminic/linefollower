/*******************************************************************************
* File Name: sen9.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen9_H) /* Pins sen9_H */
#define CY_PINS_sen9_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen9_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen9__PORT == 15 && ((sen9__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen9_Write(uint8 value);
void    sen9_SetDriveMode(uint8 mode);
uint8   sen9_ReadDataReg(void);
uint8   sen9_Read(void);
void    sen9_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen9_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen9_SetDriveMode() function.
     *  @{
     */
        #define sen9_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen9_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen9_DM_RES_UP          PIN_DM_RES_UP
        #define sen9_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen9_DM_OD_LO           PIN_DM_OD_LO
        #define sen9_DM_OD_HI           PIN_DM_OD_HI
        #define sen9_DM_STRONG          PIN_DM_STRONG
        #define sen9_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen9_MASK               sen9__MASK
#define sen9_SHIFT              sen9__SHIFT
#define sen9_WIDTH              1u

/* Interrupt constants */
#if defined(sen9__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen9_SetInterruptMode() function.
     *  @{
     */
        #define sen9_INTR_NONE      (uint16)(0x0000u)
        #define sen9_INTR_RISING    (uint16)(0x0001u)
        #define sen9_INTR_FALLING   (uint16)(0x0002u)
        #define sen9_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen9_INTR_MASK      (0x01u) 
#endif /* (sen9__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen9_PS                     (* (reg8 *) sen9__PS)
/* Data Register */
#define sen9_DR                     (* (reg8 *) sen9__DR)
/* Port Number */
#define sen9_PRT_NUM                (* (reg8 *) sen9__PRT) 
/* Connect to Analog Globals */                                                  
#define sen9_AG                     (* (reg8 *) sen9__AG)                       
/* Analog MUX bux enable */
#define sen9_AMUX                   (* (reg8 *) sen9__AMUX) 
/* Bidirectional Enable */                                                        
#define sen9_BIE                    (* (reg8 *) sen9__BIE)
/* Bit-mask for Aliased Register Access */
#define sen9_BIT_MASK               (* (reg8 *) sen9__BIT_MASK)
/* Bypass Enable */
#define sen9_BYP                    (* (reg8 *) sen9__BYP)
/* Port wide control signals */                                                   
#define sen9_CTL                    (* (reg8 *) sen9__CTL)
/* Drive Modes */
#define sen9_DM0                    (* (reg8 *) sen9__DM0) 
#define sen9_DM1                    (* (reg8 *) sen9__DM1)
#define sen9_DM2                    (* (reg8 *) sen9__DM2) 
/* Input Buffer Disable Override */
#define sen9_INP_DIS                (* (reg8 *) sen9__INP_DIS)
/* LCD Common or Segment Drive */
#define sen9_LCD_COM_SEG            (* (reg8 *) sen9__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen9_LCD_EN                 (* (reg8 *) sen9__LCD_EN)
/* Slew Rate Control */
#define sen9_SLW                    (* (reg8 *) sen9__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen9_PRTDSI__CAPS_SEL       (* (reg8 *) sen9__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen9_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen9__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen9_PRTDSI__OE_SEL0        (* (reg8 *) sen9__PRTDSI__OE_SEL0) 
#define sen9_PRTDSI__OE_SEL1        (* (reg8 *) sen9__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen9_PRTDSI__OUT_SEL0       (* (reg8 *) sen9__PRTDSI__OUT_SEL0) 
#define sen9_PRTDSI__OUT_SEL1       (* (reg8 *) sen9__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen9_PRTDSI__SYNC_OUT       (* (reg8 *) sen9__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen9__SIO_CFG)
    #define sen9_SIO_HYST_EN        (* (reg8 *) sen9__SIO_HYST_EN)
    #define sen9_SIO_REG_HIFREQ     (* (reg8 *) sen9__SIO_REG_HIFREQ)
    #define sen9_SIO_CFG            (* (reg8 *) sen9__SIO_CFG)
    #define sen9_SIO_DIFF           (* (reg8 *) sen9__SIO_DIFF)
#endif /* (sen9__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen9__INTSTAT)
    #define sen9_INTSTAT            (* (reg8 *) sen9__INTSTAT)
    #define sen9_SNAP               (* (reg8 *) sen9__SNAP)
    
	#define sen9_0_INTTYPE_REG 		(* (reg8 *) sen9__0__INTTYPE)
#endif /* (sen9__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen9_H */


/* [] END OF FILE */
