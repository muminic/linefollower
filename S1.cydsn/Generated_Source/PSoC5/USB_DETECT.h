/*******************************************************************************
* File Name: USB_DETECT.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_USB_DETECT_H) /* Pins USB_DETECT_H */
#define CY_PINS_USB_DETECT_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "USB_DETECT_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 USB_DETECT__PORT == 15 && ((USB_DETECT__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    USB_DETECT_Write(uint8 value);
void    USB_DETECT_SetDriveMode(uint8 mode);
uint8   USB_DETECT_ReadDataReg(void);
uint8   USB_DETECT_Read(void);
void    USB_DETECT_SetInterruptMode(uint16 position, uint16 mode);
uint8   USB_DETECT_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the USB_DETECT_SetDriveMode() function.
     *  @{
     */
        #define USB_DETECT_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define USB_DETECT_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define USB_DETECT_DM_RES_UP          PIN_DM_RES_UP
        #define USB_DETECT_DM_RES_DWN         PIN_DM_RES_DWN
        #define USB_DETECT_DM_OD_LO           PIN_DM_OD_LO
        #define USB_DETECT_DM_OD_HI           PIN_DM_OD_HI
        #define USB_DETECT_DM_STRONG          PIN_DM_STRONG
        #define USB_DETECT_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define USB_DETECT_MASK               USB_DETECT__MASK
#define USB_DETECT_SHIFT              USB_DETECT__SHIFT
#define USB_DETECT_WIDTH              1u

/* Interrupt constants */
#if defined(USB_DETECT__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in USB_DETECT_SetInterruptMode() function.
     *  @{
     */
        #define USB_DETECT_INTR_NONE      (uint16)(0x0000u)
        #define USB_DETECT_INTR_RISING    (uint16)(0x0001u)
        #define USB_DETECT_INTR_FALLING   (uint16)(0x0002u)
        #define USB_DETECT_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define USB_DETECT_INTR_MASK      (0x01u) 
#endif /* (USB_DETECT__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define USB_DETECT_PS                     (* (reg8 *) USB_DETECT__PS)
/* Data Register */
#define USB_DETECT_DR                     (* (reg8 *) USB_DETECT__DR)
/* Port Number */
#define USB_DETECT_PRT_NUM                (* (reg8 *) USB_DETECT__PRT) 
/* Connect to Analog Globals */                                                  
#define USB_DETECT_AG                     (* (reg8 *) USB_DETECT__AG)                       
/* Analog MUX bux enable */
#define USB_DETECT_AMUX                   (* (reg8 *) USB_DETECT__AMUX) 
/* Bidirectional Enable */                                                        
#define USB_DETECT_BIE                    (* (reg8 *) USB_DETECT__BIE)
/* Bit-mask for Aliased Register Access */
#define USB_DETECT_BIT_MASK               (* (reg8 *) USB_DETECT__BIT_MASK)
/* Bypass Enable */
#define USB_DETECT_BYP                    (* (reg8 *) USB_DETECT__BYP)
/* Port wide control signals */                                                   
#define USB_DETECT_CTL                    (* (reg8 *) USB_DETECT__CTL)
/* Drive Modes */
#define USB_DETECT_DM0                    (* (reg8 *) USB_DETECT__DM0) 
#define USB_DETECT_DM1                    (* (reg8 *) USB_DETECT__DM1)
#define USB_DETECT_DM2                    (* (reg8 *) USB_DETECT__DM2) 
/* Input Buffer Disable Override */
#define USB_DETECT_INP_DIS                (* (reg8 *) USB_DETECT__INP_DIS)
/* LCD Common or Segment Drive */
#define USB_DETECT_LCD_COM_SEG            (* (reg8 *) USB_DETECT__LCD_COM_SEG)
/* Enable Segment LCD */
#define USB_DETECT_LCD_EN                 (* (reg8 *) USB_DETECT__LCD_EN)
/* Slew Rate Control */
#define USB_DETECT_SLW                    (* (reg8 *) USB_DETECT__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define USB_DETECT_PRTDSI__CAPS_SEL       (* (reg8 *) USB_DETECT__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define USB_DETECT_PRTDSI__DBL_SYNC_IN    (* (reg8 *) USB_DETECT__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define USB_DETECT_PRTDSI__OE_SEL0        (* (reg8 *) USB_DETECT__PRTDSI__OE_SEL0) 
#define USB_DETECT_PRTDSI__OE_SEL1        (* (reg8 *) USB_DETECT__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define USB_DETECT_PRTDSI__OUT_SEL0       (* (reg8 *) USB_DETECT__PRTDSI__OUT_SEL0) 
#define USB_DETECT_PRTDSI__OUT_SEL1       (* (reg8 *) USB_DETECT__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define USB_DETECT_PRTDSI__SYNC_OUT       (* (reg8 *) USB_DETECT__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(USB_DETECT__SIO_CFG)
    #define USB_DETECT_SIO_HYST_EN        (* (reg8 *) USB_DETECT__SIO_HYST_EN)
    #define USB_DETECT_SIO_REG_HIFREQ     (* (reg8 *) USB_DETECT__SIO_REG_HIFREQ)
    #define USB_DETECT_SIO_CFG            (* (reg8 *) USB_DETECT__SIO_CFG)
    #define USB_DETECT_SIO_DIFF           (* (reg8 *) USB_DETECT__SIO_DIFF)
#endif /* (USB_DETECT__SIO_CFG) */

/* Interrupt Registers */
#if defined(USB_DETECT__INTSTAT)
    #define USB_DETECT_INTSTAT            (* (reg8 *) USB_DETECT__INTSTAT)
    #define USB_DETECT_SNAP               (* (reg8 *) USB_DETECT__SNAP)
    
	#define USB_DETECT_0_INTTYPE_REG 		(* (reg8 *) USB_DETECT__0__INTTYPE)
#endif /* (USB_DETECT__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_USB_DETECT_H */


/* [] END OF FILE */
