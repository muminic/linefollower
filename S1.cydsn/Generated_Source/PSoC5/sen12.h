/*******************************************************************************
* File Name: sen12.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen12_H) /* Pins sen12_H */
#define CY_PINS_sen12_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen12_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen12__PORT == 15 && ((sen12__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen12_Write(uint8 value);
void    sen12_SetDriveMode(uint8 mode);
uint8   sen12_ReadDataReg(void);
uint8   sen12_Read(void);
void    sen12_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen12_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen12_SetDriveMode() function.
     *  @{
     */
        #define sen12_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen12_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen12_DM_RES_UP          PIN_DM_RES_UP
        #define sen12_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen12_DM_OD_LO           PIN_DM_OD_LO
        #define sen12_DM_OD_HI           PIN_DM_OD_HI
        #define sen12_DM_STRONG          PIN_DM_STRONG
        #define sen12_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen12_MASK               sen12__MASK
#define sen12_SHIFT              sen12__SHIFT
#define sen12_WIDTH              1u

/* Interrupt constants */
#if defined(sen12__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen12_SetInterruptMode() function.
     *  @{
     */
        #define sen12_INTR_NONE      (uint16)(0x0000u)
        #define sen12_INTR_RISING    (uint16)(0x0001u)
        #define sen12_INTR_FALLING   (uint16)(0x0002u)
        #define sen12_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen12_INTR_MASK      (0x01u) 
#endif /* (sen12__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen12_PS                     (* (reg8 *) sen12__PS)
/* Data Register */
#define sen12_DR                     (* (reg8 *) sen12__DR)
/* Port Number */
#define sen12_PRT_NUM                (* (reg8 *) sen12__PRT) 
/* Connect to Analog Globals */                                                  
#define sen12_AG                     (* (reg8 *) sen12__AG)                       
/* Analog MUX bux enable */
#define sen12_AMUX                   (* (reg8 *) sen12__AMUX) 
/* Bidirectional Enable */                                                        
#define sen12_BIE                    (* (reg8 *) sen12__BIE)
/* Bit-mask for Aliased Register Access */
#define sen12_BIT_MASK               (* (reg8 *) sen12__BIT_MASK)
/* Bypass Enable */
#define sen12_BYP                    (* (reg8 *) sen12__BYP)
/* Port wide control signals */                                                   
#define sen12_CTL                    (* (reg8 *) sen12__CTL)
/* Drive Modes */
#define sen12_DM0                    (* (reg8 *) sen12__DM0) 
#define sen12_DM1                    (* (reg8 *) sen12__DM1)
#define sen12_DM2                    (* (reg8 *) sen12__DM2) 
/* Input Buffer Disable Override */
#define sen12_INP_DIS                (* (reg8 *) sen12__INP_DIS)
/* LCD Common or Segment Drive */
#define sen12_LCD_COM_SEG            (* (reg8 *) sen12__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen12_LCD_EN                 (* (reg8 *) sen12__LCD_EN)
/* Slew Rate Control */
#define sen12_SLW                    (* (reg8 *) sen12__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen12_PRTDSI__CAPS_SEL       (* (reg8 *) sen12__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen12_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen12__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen12_PRTDSI__OE_SEL0        (* (reg8 *) sen12__PRTDSI__OE_SEL0) 
#define sen12_PRTDSI__OE_SEL1        (* (reg8 *) sen12__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen12_PRTDSI__OUT_SEL0       (* (reg8 *) sen12__PRTDSI__OUT_SEL0) 
#define sen12_PRTDSI__OUT_SEL1       (* (reg8 *) sen12__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen12_PRTDSI__SYNC_OUT       (* (reg8 *) sen12__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen12__SIO_CFG)
    #define sen12_SIO_HYST_EN        (* (reg8 *) sen12__SIO_HYST_EN)
    #define sen12_SIO_REG_HIFREQ     (* (reg8 *) sen12__SIO_REG_HIFREQ)
    #define sen12_SIO_CFG            (* (reg8 *) sen12__SIO_CFG)
    #define sen12_SIO_DIFF           (* (reg8 *) sen12__SIO_DIFF)
#endif /* (sen12__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen12__INTSTAT)
    #define sen12_INTSTAT            (* (reg8 *) sen12__INTSTAT)
    #define sen12_SNAP               (* (reg8 *) sen12__SNAP)
    
	#define sen12_0_INTTYPE_REG 		(* (reg8 *) sen12__0__INTTYPE)
#endif /* (sen12__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen12_H */


/* [] END OF FILE */
