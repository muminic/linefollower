/*******************************************************************************
* File Name: TIMER_MS_PM.c
* Version 2.80
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "TIMER_MS.h"

static TIMER_MS_backupStruct TIMER_MS_backup;


/*******************************************************************************
* Function Name: TIMER_MS_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_MS_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void TIMER_MS_SaveConfig(void) 
{
    #if (!TIMER_MS_UsingFixedFunction)
        TIMER_MS_backup.TimerUdb = TIMER_MS_ReadCounter();
        TIMER_MS_backup.InterruptMaskValue = TIMER_MS_STATUS_MASK;
        #if (TIMER_MS_UsingHWCaptureCounter)
            TIMER_MS_backup.TimerCaptureCounter = TIMER_MS_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!TIMER_MS_UDB_CONTROL_REG_REMOVED)
            TIMER_MS_backup.TimerControlRegister = TIMER_MS_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: TIMER_MS_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_MS_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void TIMER_MS_RestoreConfig(void) 
{   
    #if (!TIMER_MS_UsingFixedFunction)

        TIMER_MS_WriteCounter(TIMER_MS_backup.TimerUdb);
        TIMER_MS_STATUS_MASK =TIMER_MS_backup.InterruptMaskValue;
        #if (TIMER_MS_UsingHWCaptureCounter)
            TIMER_MS_SetCaptureCount(TIMER_MS_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!TIMER_MS_UDB_CONTROL_REG_REMOVED)
            TIMER_MS_WriteControlRegister(TIMER_MS_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: TIMER_MS_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_MS_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void TIMER_MS_Sleep(void) 
{
    #if(!TIMER_MS_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(TIMER_MS_CTRL_ENABLE == (TIMER_MS_CONTROL & TIMER_MS_CTRL_ENABLE))
        {
            /* Timer is enabled */
            TIMER_MS_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            TIMER_MS_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    TIMER_MS_Stop();
    TIMER_MS_SaveConfig();
}


/*******************************************************************************
* Function Name: TIMER_MS_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_MS_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void TIMER_MS_Wakeup(void) 
{
    TIMER_MS_RestoreConfig();
    #if(!TIMER_MS_UDB_CONTROL_REG_REMOVED)
        if(TIMER_MS_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                TIMER_MS_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
