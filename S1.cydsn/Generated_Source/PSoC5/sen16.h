/*******************************************************************************
* File Name: sen16.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen16_H) /* Pins sen16_H */
#define CY_PINS_sen16_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen16_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen16__PORT == 15 && ((sen16__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen16_Write(uint8 value);
void    sen16_SetDriveMode(uint8 mode);
uint8   sen16_ReadDataReg(void);
uint8   sen16_Read(void);
void    sen16_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen16_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen16_SetDriveMode() function.
     *  @{
     */
        #define sen16_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen16_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen16_DM_RES_UP          PIN_DM_RES_UP
        #define sen16_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen16_DM_OD_LO           PIN_DM_OD_LO
        #define sen16_DM_OD_HI           PIN_DM_OD_HI
        #define sen16_DM_STRONG          PIN_DM_STRONG
        #define sen16_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen16_MASK               sen16__MASK
#define sen16_SHIFT              sen16__SHIFT
#define sen16_WIDTH              1u

/* Interrupt constants */
#if defined(sen16__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen16_SetInterruptMode() function.
     *  @{
     */
        #define sen16_INTR_NONE      (uint16)(0x0000u)
        #define sen16_INTR_RISING    (uint16)(0x0001u)
        #define sen16_INTR_FALLING   (uint16)(0x0002u)
        #define sen16_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen16_INTR_MASK      (0x01u) 
#endif /* (sen16__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen16_PS                     (* (reg8 *) sen16__PS)
/* Data Register */
#define sen16_DR                     (* (reg8 *) sen16__DR)
/* Port Number */
#define sen16_PRT_NUM                (* (reg8 *) sen16__PRT) 
/* Connect to Analog Globals */                                                  
#define sen16_AG                     (* (reg8 *) sen16__AG)                       
/* Analog MUX bux enable */
#define sen16_AMUX                   (* (reg8 *) sen16__AMUX) 
/* Bidirectional Enable */                                                        
#define sen16_BIE                    (* (reg8 *) sen16__BIE)
/* Bit-mask for Aliased Register Access */
#define sen16_BIT_MASK               (* (reg8 *) sen16__BIT_MASK)
/* Bypass Enable */
#define sen16_BYP                    (* (reg8 *) sen16__BYP)
/* Port wide control signals */                                                   
#define sen16_CTL                    (* (reg8 *) sen16__CTL)
/* Drive Modes */
#define sen16_DM0                    (* (reg8 *) sen16__DM0) 
#define sen16_DM1                    (* (reg8 *) sen16__DM1)
#define sen16_DM2                    (* (reg8 *) sen16__DM2) 
/* Input Buffer Disable Override */
#define sen16_INP_DIS                (* (reg8 *) sen16__INP_DIS)
/* LCD Common or Segment Drive */
#define sen16_LCD_COM_SEG            (* (reg8 *) sen16__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen16_LCD_EN                 (* (reg8 *) sen16__LCD_EN)
/* Slew Rate Control */
#define sen16_SLW                    (* (reg8 *) sen16__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen16_PRTDSI__CAPS_SEL       (* (reg8 *) sen16__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen16_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen16__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen16_PRTDSI__OE_SEL0        (* (reg8 *) sen16__PRTDSI__OE_SEL0) 
#define sen16_PRTDSI__OE_SEL1        (* (reg8 *) sen16__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen16_PRTDSI__OUT_SEL0       (* (reg8 *) sen16__PRTDSI__OUT_SEL0) 
#define sen16_PRTDSI__OUT_SEL1       (* (reg8 *) sen16__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen16_PRTDSI__SYNC_OUT       (* (reg8 *) sen16__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen16__SIO_CFG)
    #define sen16_SIO_HYST_EN        (* (reg8 *) sen16__SIO_HYST_EN)
    #define sen16_SIO_REG_HIFREQ     (* (reg8 *) sen16__SIO_REG_HIFREQ)
    #define sen16_SIO_CFG            (* (reg8 *) sen16__SIO_CFG)
    #define sen16_SIO_DIFF           (* (reg8 *) sen16__SIO_DIFF)
#endif /* (sen16__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen16__INTSTAT)
    #define sen16_INTSTAT            (* (reg8 *) sen16__INTSTAT)
    #define sen16_SNAP               (* (reg8 *) sen16__SNAP)
    
	#define sen16_0_INTTYPE_REG 		(* (reg8 *) sen16__0__INTTYPE)
#endif /* (sen16__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen16_H */


/* [] END OF FILE */
