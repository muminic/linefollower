/*******************************************************************************
* File Name: TIMER_US_PM.c
* Version 2.70
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "TIMER_US.h"

static TIMER_US_backupStruct TIMER_US_backup;


/*******************************************************************************
* Function Name: TIMER_US_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_US_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void TIMER_US_SaveConfig(void) 
{
    #if (!TIMER_US_UsingFixedFunction)
        TIMER_US_backup.TimerUdb = TIMER_US_ReadCounter();
        TIMER_US_backup.InterruptMaskValue = TIMER_US_STATUS_MASK;
        #if (TIMER_US_UsingHWCaptureCounter)
            TIMER_US_backup.TimerCaptureCounter = TIMER_US_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!TIMER_US_UDB_CONTROL_REG_REMOVED)
            TIMER_US_backup.TimerControlRegister = TIMER_US_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: TIMER_US_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_US_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void TIMER_US_RestoreConfig(void) 
{   
    #if (!TIMER_US_UsingFixedFunction)

        TIMER_US_WriteCounter(TIMER_US_backup.TimerUdb);
        TIMER_US_STATUS_MASK =TIMER_US_backup.InterruptMaskValue;
        #if (TIMER_US_UsingHWCaptureCounter)
            TIMER_US_SetCaptureCount(TIMER_US_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!TIMER_US_UDB_CONTROL_REG_REMOVED)
            TIMER_US_WriteControlRegister(TIMER_US_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: TIMER_US_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_US_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void TIMER_US_Sleep(void) 
{
    #if(!TIMER_US_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(TIMER_US_CTRL_ENABLE == (TIMER_US_CONTROL & TIMER_US_CTRL_ENABLE))
        {
            /* Timer is enabled */
            TIMER_US_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            TIMER_US_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    TIMER_US_Stop();
    TIMER_US_SaveConfig();
}


/*******************************************************************************
* Function Name: TIMER_US_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TIMER_US_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void TIMER_US_Wakeup(void) 
{
    TIMER_US_RestoreConfig();
    #if(!TIMER_US_UDB_CONTROL_REG_REMOVED)
        if(TIMER_US_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                TIMER_US_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
