/*******************************************************************************
* File Name: sen6.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen6_H) /* Pins sen6_H */
#define CY_PINS_sen6_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen6_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen6__PORT == 15 && ((sen6__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen6_Write(uint8 value);
void    sen6_SetDriveMode(uint8 mode);
uint8   sen6_ReadDataReg(void);
uint8   sen6_Read(void);
void    sen6_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen6_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen6_SetDriveMode() function.
     *  @{
     */
        #define sen6_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen6_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen6_DM_RES_UP          PIN_DM_RES_UP
        #define sen6_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen6_DM_OD_LO           PIN_DM_OD_LO
        #define sen6_DM_OD_HI           PIN_DM_OD_HI
        #define sen6_DM_STRONG          PIN_DM_STRONG
        #define sen6_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen6_MASK               sen6__MASK
#define sen6_SHIFT              sen6__SHIFT
#define sen6_WIDTH              1u

/* Interrupt constants */
#if defined(sen6__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen6_SetInterruptMode() function.
     *  @{
     */
        #define sen6_INTR_NONE      (uint16)(0x0000u)
        #define sen6_INTR_RISING    (uint16)(0x0001u)
        #define sen6_INTR_FALLING   (uint16)(0x0002u)
        #define sen6_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen6_INTR_MASK      (0x01u) 
#endif /* (sen6__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen6_PS                     (* (reg8 *) sen6__PS)
/* Data Register */
#define sen6_DR                     (* (reg8 *) sen6__DR)
/* Port Number */
#define sen6_PRT_NUM                (* (reg8 *) sen6__PRT) 
/* Connect to Analog Globals */                                                  
#define sen6_AG                     (* (reg8 *) sen6__AG)                       
/* Analog MUX bux enable */
#define sen6_AMUX                   (* (reg8 *) sen6__AMUX) 
/* Bidirectional Enable */                                                        
#define sen6_BIE                    (* (reg8 *) sen6__BIE)
/* Bit-mask for Aliased Register Access */
#define sen6_BIT_MASK               (* (reg8 *) sen6__BIT_MASK)
/* Bypass Enable */
#define sen6_BYP                    (* (reg8 *) sen6__BYP)
/* Port wide control signals */                                                   
#define sen6_CTL                    (* (reg8 *) sen6__CTL)
/* Drive Modes */
#define sen6_DM0                    (* (reg8 *) sen6__DM0) 
#define sen6_DM1                    (* (reg8 *) sen6__DM1)
#define sen6_DM2                    (* (reg8 *) sen6__DM2) 
/* Input Buffer Disable Override */
#define sen6_INP_DIS                (* (reg8 *) sen6__INP_DIS)
/* LCD Common or Segment Drive */
#define sen6_LCD_COM_SEG            (* (reg8 *) sen6__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen6_LCD_EN                 (* (reg8 *) sen6__LCD_EN)
/* Slew Rate Control */
#define sen6_SLW                    (* (reg8 *) sen6__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen6_PRTDSI__CAPS_SEL       (* (reg8 *) sen6__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen6_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen6__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen6_PRTDSI__OE_SEL0        (* (reg8 *) sen6__PRTDSI__OE_SEL0) 
#define sen6_PRTDSI__OE_SEL1        (* (reg8 *) sen6__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen6_PRTDSI__OUT_SEL0       (* (reg8 *) sen6__PRTDSI__OUT_SEL0) 
#define sen6_PRTDSI__OUT_SEL1       (* (reg8 *) sen6__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen6_PRTDSI__SYNC_OUT       (* (reg8 *) sen6__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen6__SIO_CFG)
    #define sen6_SIO_HYST_EN        (* (reg8 *) sen6__SIO_HYST_EN)
    #define sen6_SIO_REG_HIFREQ     (* (reg8 *) sen6__SIO_REG_HIFREQ)
    #define sen6_SIO_CFG            (* (reg8 *) sen6__SIO_CFG)
    #define sen6_SIO_DIFF           (* (reg8 *) sen6__SIO_DIFF)
#endif /* (sen6__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen6__INTSTAT)
    #define sen6_INTSTAT            (* (reg8 *) sen6__INTSTAT)
    #define sen6_SNAP               (* (reg8 *) sen6__SNAP)
    
	#define sen6_0_INTTYPE_REG 		(* (reg8 *) sen6__0__INTTYPE)
#endif /* (sen6__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen6_H */


/* [] END OF FILE */
