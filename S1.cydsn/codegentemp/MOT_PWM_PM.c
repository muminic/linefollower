/*******************************************************************************
* File Name: MOT_PWM_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "MOT_PWM.h"

static MOT_PWM_backupStruct MOT_PWM_backup;


/*******************************************************************************
* Function Name: MOT_PWM_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  MOT_PWM_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void MOT_PWM_SaveConfig(void) 
{

    #if(!MOT_PWM_UsingFixedFunction)
        #if(!MOT_PWM_PWMModeIsCenterAligned)
            MOT_PWM_backup.PWMPeriod = MOT_PWM_ReadPeriod();
        #endif /* (!MOT_PWM_PWMModeIsCenterAligned) */
        MOT_PWM_backup.PWMUdb = MOT_PWM_ReadCounter();
        #if (MOT_PWM_UseStatus)
            MOT_PWM_backup.InterruptMaskValue = MOT_PWM_STATUS_MASK;
        #endif /* (MOT_PWM_UseStatus) */

        #if(MOT_PWM_DeadBandMode == MOT_PWM__B_PWM__DBM_256_CLOCKS || \
            MOT_PWM_DeadBandMode == MOT_PWM__B_PWM__DBM_2_4_CLOCKS)
            MOT_PWM_backup.PWMdeadBandValue = MOT_PWM_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(MOT_PWM_KillModeMinTime)
             MOT_PWM_backup.PWMKillCounterPeriod = MOT_PWM_ReadKillTime();
        #endif /* (MOT_PWM_KillModeMinTime) */

        #if(MOT_PWM_UseControl)
            MOT_PWM_backup.PWMControlRegister = MOT_PWM_ReadControlRegister();
        #endif /* (MOT_PWM_UseControl) */
    #endif  /* (!MOT_PWM_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: MOT_PWM_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  MOT_PWM_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void MOT_PWM_RestoreConfig(void) 
{
        #if(!MOT_PWM_UsingFixedFunction)
            #if(!MOT_PWM_PWMModeIsCenterAligned)
                MOT_PWM_WritePeriod(MOT_PWM_backup.PWMPeriod);
            #endif /* (!MOT_PWM_PWMModeIsCenterAligned) */

            MOT_PWM_WriteCounter(MOT_PWM_backup.PWMUdb);

            #if (MOT_PWM_UseStatus)
                MOT_PWM_STATUS_MASK = MOT_PWM_backup.InterruptMaskValue;
            #endif /* (MOT_PWM_UseStatus) */

            #if(MOT_PWM_DeadBandMode == MOT_PWM__B_PWM__DBM_256_CLOCKS || \
                MOT_PWM_DeadBandMode == MOT_PWM__B_PWM__DBM_2_4_CLOCKS)
                MOT_PWM_WriteDeadTime(MOT_PWM_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(MOT_PWM_KillModeMinTime)
                MOT_PWM_WriteKillTime(MOT_PWM_backup.PWMKillCounterPeriod);
            #endif /* (MOT_PWM_KillModeMinTime) */

            #if(MOT_PWM_UseControl)
                MOT_PWM_WriteControlRegister(MOT_PWM_backup.PWMControlRegister);
            #endif /* (MOT_PWM_UseControl) */
        #endif  /* (!MOT_PWM_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: MOT_PWM_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  MOT_PWM_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void MOT_PWM_Sleep(void) 
{
    #if(MOT_PWM_UseControl)
        if(MOT_PWM_CTRL_ENABLE == (MOT_PWM_CONTROL & MOT_PWM_CTRL_ENABLE))
        {
            /*Component is enabled */
            MOT_PWM_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            MOT_PWM_backup.PWMEnableState = 0u;
        }
    #endif /* (MOT_PWM_UseControl) */

    /* Stop component */
    MOT_PWM_Stop();

    /* Save registers configuration */
    MOT_PWM_SaveConfig();
}


/*******************************************************************************
* Function Name: MOT_PWM_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  MOT_PWM_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void MOT_PWM_Wakeup(void) 
{
     /* Restore registers values */
    MOT_PWM_RestoreConfig();

    if(MOT_PWM_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        MOT_PWM_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
