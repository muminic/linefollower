/*******************************************************************************
* File Name: sen2.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen2_H) /* Pins sen2_H */
#define CY_PINS_sen2_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen2_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen2__PORT == 15 && ((sen2__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen2_Write(uint8 value);
void    sen2_SetDriveMode(uint8 mode);
uint8   sen2_ReadDataReg(void);
uint8   sen2_Read(void);
void    sen2_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen2_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen2_SetDriveMode() function.
     *  @{
     */
        #define sen2_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen2_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen2_DM_RES_UP          PIN_DM_RES_UP
        #define sen2_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen2_DM_OD_LO           PIN_DM_OD_LO
        #define sen2_DM_OD_HI           PIN_DM_OD_HI
        #define sen2_DM_STRONG          PIN_DM_STRONG
        #define sen2_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen2_MASK               sen2__MASK
#define sen2_SHIFT              sen2__SHIFT
#define sen2_WIDTH              1u

/* Interrupt constants */
#if defined(sen2__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen2_SetInterruptMode() function.
     *  @{
     */
        #define sen2_INTR_NONE      (uint16)(0x0000u)
        #define sen2_INTR_RISING    (uint16)(0x0001u)
        #define sen2_INTR_FALLING   (uint16)(0x0002u)
        #define sen2_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen2_INTR_MASK      (0x01u) 
#endif /* (sen2__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen2_PS                     (* (reg8 *) sen2__PS)
/* Data Register */
#define sen2_DR                     (* (reg8 *) sen2__DR)
/* Port Number */
#define sen2_PRT_NUM                (* (reg8 *) sen2__PRT) 
/* Connect to Analog Globals */                                                  
#define sen2_AG                     (* (reg8 *) sen2__AG)                       
/* Analog MUX bux enable */
#define sen2_AMUX                   (* (reg8 *) sen2__AMUX) 
/* Bidirectional Enable */                                                        
#define sen2_BIE                    (* (reg8 *) sen2__BIE)
/* Bit-mask for Aliased Register Access */
#define sen2_BIT_MASK               (* (reg8 *) sen2__BIT_MASK)
/* Bypass Enable */
#define sen2_BYP                    (* (reg8 *) sen2__BYP)
/* Port wide control signals */                                                   
#define sen2_CTL                    (* (reg8 *) sen2__CTL)
/* Drive Modes */
#define sen2_DM0                    (* (reg8 *) sen2__DM0) 
#define sen2_DM1                    (* (reg8 *) sen2__DM1)
#define sen2_DM2                    (* (reg8 *) sen2__DM2) 
/* Input Buffer Disable Override */
#define sen2_INP_DIS                (* (reg8 *) sen2__INP_DIS)
/* LCD Common or Segment Drive */
#define sen2_LCD_COM_SEG            (* (reg8 *) sen2__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen2_LCD_EN                 (* (reg8 *) sen2__LCD_EN)
/* Slew Rate Control */
#define sen2_SLW                    (* (reg8 *) sen2__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen2_PRTDSI__CAPS_SEL       (* (reg8 *) sen2__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen2_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen2__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen2_PRTDSI__OE_SEL0        (* (reg8 *) sen2__PRTDSI__OE_SEL0) 
#define sen2_PRTDSI__OE_SEL1        (* (reg8 *) sen2__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen2_PRTDSI__OUT_SEL0       (* (reg8 *) sen2__PRTDSI__OUT_SEL0) 
#define sen2_PRTDSI__OUT_SEL1       (* (reg8 *) sen2__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen2_PRTDSI__SYNC_OUT       (* (reg8 *) sen2__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen2__SIO_CFG)
    #define sen2_SIO_HYST_EN        (* (reg8 *) sen2__SIO_HYST_EN)
    #define sen2_SIO_REG_HIFREQ     (* (reg8 *) sen2__SIO_REG_HIFREQ)
    #define sen2_SIO_CFG            (* (reg8 *) sen2__SIO_CFG)
    #define sen2_SIO_DIFF           (* (reg8 *) sen2__SIO_DIFF)
#endif /* (sen2__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen2__INTSTAT)
    #define sen2_INTSTAT            (* (reg8 *) sen2__INTSTAT)
    #define sen2_SNAP               (* (reg8 *) sen2__SNAP)
    
	#define sen2_0_INTTYPE_REG 		(* (reg8 *) sen2__0__INTTYPE)
#endif /* (sen2__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen2_H */


/* [] END OF FILE */
