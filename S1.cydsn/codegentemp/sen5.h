/*******************************************************************************
* File Name: sen5.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen5_H) /* Pins sen5_H */
#define CY_PINS_sen5_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen5_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen5__PORT == 15 && ((sen5__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen5_Write(uint8 value);
void    sen5_SetDriveMode(uint8 mode);
uint8   sen5_ReadDataReg(void);
uint8   sen5_Read(void);
void    sen5_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen5_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen5_SetDriveMode() function.
     *  @{
     */
        #define sen5_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen5_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen5_DM_RES_UP          PIN_DM_RES_UP
        #define sen5_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen5_DM_OD_LO           PIN_DM_OD_LO
        #define sen5_DM_OD_HI           PIN_DM_OD_HI
        #define sen5_DM_STRONG          PIN_DM_STRONG
        #define sen5_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen5_MASK               sen5__MASK
#define sen5_SHIFT              sen5__SHIFT
#define sen5_WIDTH              1u

/* Interrupt constants */
#if defined(sen5__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen5_SetInterruptMode() function.
     *  @{
     */
        #define sen5_INTR_NONE      (uint16)(0x0000u)
        #define sen5_INTR_RISING    (uint16)(0x0001u)
        #define sen5_INTR_FALLING   (uint16)(0x0002u)
        #define sen5_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen5_INTR_MASK      (0x01u) 
#endif /* (sen5__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen5_PS                     (* (reg8 *) sen5__PS)
/* Data Register */
#define sen5_DR                     (* (reg8 *) sen5__DR)
/* Port Number */
#define sen5_PRT_NUM                (* (reg8 *) sen5__PRT) 
/* Connect to Analog Globals */                                                  
#define sen5_AG                     (* (reg8 *) sen5__AG)                       
/* Analog MUX bux enable */
#define sen5_AMUX                   (* (reg8 *) sen5__AMUX) 
/* Bidirectional Enable */                                                        
#define sen5_BIE                    (* (reg8 *) sen5__BIE)
/* Bit-mask for Aliased Register Access */
#define sen5_BIT_MASK               (* (reg8 *) sen5__BIT_MASK)
/* Bypass Enable */
#define sen5_BYP                    (* (reg8 *) sen5__BYP)
/* Port wide control signals */                                                   
#define sen5_CTL                    (* (reg8 *) sen5__CTL)
/* Drive Modes */
#define sen5_DM0                    (* (reg8 *) sen5__DM0) 
#define sen5_DM1                    (* (reg8 *) sen5__DM1)
#define sen5_DM2                    (* (reg8 *) sen5__DM2) 
/* Input Buffer Disable Override */
#define sen5_INP_DIS                (* (reg8 *) sen5__INP_DIS)
/* LCD Common or Segment Drive */
#define sen5_LCD_COM_SEG            (* (reg8 *) sen5__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen5_LCD_EN                 (* (reg8 *) sen5__LCD_EN)
/* Slew Rate Control */
#define sen5_SLW                    (* (reg8 *) sen5__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen5_PRTDSI__CAPS_SEL       (* (reg8 *) sen5__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen5_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen5__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen5_PRTDSI__OE_SEL0        (* (reg8 *) sen5__PRTDSI__OE_SEL0) 
#define sen5_PRTDSI__OE_SEL1        (* (reg8 *) sen5__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen5_PRTDSI__OUT_SEL0       (* (reg8 *) sen5__PRTDSI__OUT_SEL0) 
#define sen5_PRTDSI__OUT_SEL1       (* (reg8 *) sen5__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen5_PRTDSI__SYNC_OUT       (* (reg8 *) sen5__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen5__SIO_CFG)
    #define sen5_SIO_HYST_EN        (* (reg8 *) sen5__SIO_HYST_EN)
    #define sen5_SIO_REG_HIFREQ     (* (reg8 *) sen5__SIO_REG_HIFREQ)
    #define sen5_SIO_CFG            (* (reg8 *) sen5__SIO_CFG)
    #define sen5_SIO_DIFF           (* (reg8 *) sen5__SIO_DIFF)
#endif /* (sen5__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen5__INTSTAT)
    #define sen5_INTSTAT            (* (reg8 *) sen5__INTSTAT)
    #define sen5_SNAP               (* (reg8 *) sen5__SNAP)
    
	#define sen5_0_INTTYPE_REG 		(* (reg8 *) sen5__0__INTTYPE)
#endif /* (sen5__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen5_H */


/* [] END OF FILE */
