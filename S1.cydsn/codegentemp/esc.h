/*******************************************************************************
* File Name: esc.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_esc_H) /* Pins esc_H */
#define CY_PINS_esc_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "esc_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 esc__PORT == 15 && ((esc__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    esc_Write(uint8 value);
void    esc_SetDriveMode(uint8 mode);
uint8   esc_ReadDataReg(void);
uint8   esc_Read(void);
void    esc_SetInterruptMode(uint16 position, uint16 mode);
uint8   esc_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the esc_SetDriveMode() function.
     *  @{
     */
        #define esc_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define esc_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define esc_DM_RES_UP          PIN_DM_RES_UP
        #define esc_DM_RES_DWN         PIN_DM_RES_DWN
        #define esc_DM_OD_LO           PIN_DM_OD_LO
        #define esc_DM_OD_HI           PIN_DM_OD_HI
        #define esc_DM_STRONG          PIN_DM_STRONG
        #define esc_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define esc_MASK               esc__MASK
#define esc_SHIFT              esc__SHIFT
#define esc_WIDTH              1u

/* Interrupt constants */
#if defined(esc__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in esc_SetInterruptMode() function.
     *  @{
     */
        #define esc_INTR_NONE      (uint16)(0x0000u)
        #define esc_INTR_RISING    (uint16)(0x0001u)
        #define esc_INTR_FALLING   (uint16)(0x0002u)
        #define esc_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define esc_INTR_MASK      (0x01u) 
#endif /* (esc__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define esc_PS                     (* (reg8 *) esc__PS)
/* Data Register */
#define esc_DR                     (* (reg8 *) esc__DR)
/* Port Number */
#define esc_PRT_NUM                (* (reg8 *) esc__PRT) 
/* Connect to Analog Globals */                                                  
#define esc_AG                     (* (reg8 *) esc__AG)                       
/* Analog MUX bux enable */
#define esc_AMUX                   (* (reg8 *) esc__AMUX) 
/* Bidirectional Enable */                                                        
#define esc_BIE                    (* (reg8 *) esc__BIE)
/* Bit-mask for Aliased Register Access */
#define esc_BIT_MASK               (* (reg8 *) esc__BIT_MASK)
/* Bypass Enable */
#define esc_BYP                    (* (reg8 *) esc__BYP)
/* Port wide control signals */                                                   
#define esc_CTL                    (* (reg8 *) esc__CTL)
/* Drive Modes */
#define esc_DM0                    (* (reg8 *) esc__DM0) 
#define esc_DM1                    (* (reg8 *) esc__DM1)
#define esc_DM2                    (* (reg8 *) esc__DM2) 
/* Input Buffer Disable Override */
#define esc_INP_DIS                (* (reg8 *) esc__INP_DIS)
/* LCD Common or Segment Drive */
#define esc_LCD_COM_SEG            (* (reg8 *) esc__LCD_COM_SEG)
/* Enable Segment LCD */
#define esc_LCD_EN                 (* (reg8 *) esc__LCD_EN)
/* Slew Rate Control */
#define esc_SLW                    (* (reg8 *) esc__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define esc_PRTDSI__CAPS_SEL       (* (reg8 *) esc__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define esc_PRTDSI__DBL_SYNC_IN    (* (reg8 *) esc__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define esc_PRTDSI__OE_SEL0        (* (reg8 *) esc__PRTDSI__OE_SEL0) 
#define esc_PRTDSI__OE_SEL1        (* (reg8 *) esc__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define esc_PRTDSI__OUT_SEL0       (* (reg8 *) esc__PRTDSI__OUT_SEL0) 
#define esc_PRTDSI__OUT_SEL1       (* (reg8 *) esc__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define esc_PRTDSI__SYNC_OUT       (* (reg8 *) esc__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(esc__SIO_CFG)
    #define esc_SIO_HYST_EN        (* (reg8 *) esc__SIO_HYST_EN)
    #define esc_SIO_REG_HIFREQ     (* (reg8 *) esc__SIO_REG_HIFREQ)
    #define esc_SIO_CFG            (* (reg8 *) esc__SIO_CFG)
    #define esc_SIO_DIFF           (* (reg8 *) esc__SIO_DIFF)
#endif /* (esc__SIO_CFG) */

/* Interrupt Registers */
#if defined(esc__INTSTAT)
    #define esc_INTSTAT            (* (reg8 *) esc__INTSTAT)
    #define esc_SNAP               (* (reg8 *) esc__SNAP)
    
	#define esc_0_INTTYPE_REG 		(* (reg8 *) esc__0__INTTYPE)
#endif /* (esc__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_esc_H */


/* [] END OF FILE */
