/*******************************************************************************
* File Name: sen14.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen14_H) /* Pins sen14_H */
#define CY_PINS_sen14_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen14_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen14__PORT == 15 && ((sen14__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen14_Write(uint8 value);
void    sen14_SetDriveMode(uint8 mode);
uint8   sen14_ReadDataReg(void);
uint8   sen14_Read(void);
void    sen14_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen14_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen14_SetDriveMode() function.
     *  @{
     */
        #define sen14_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen14_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen14_DM_RES_UP          PIN_DM_RES_UP
        #define sen14_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen14_DM_OD_LO           PIN_DM_OD_LO
        #define sen14_DM_OD_HI           PIN_DM_OD_HI
        #define sen14_DM_STRONG          PIN_DM_STRONG
        #define sen14_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen14_MASK               sen14__MASK
#define sen14_SHIFT              sen14__SHIFT
#define sen14_WIDTH              1u

/* Interrupt constants */
#if defined(sen14__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen14_SetInterruptMode() function.
     *  @{
     */
        #define sen14_INTR_NONE      (uint16)(0x0000u)
        #define sen14_INTR_RISING    (uint16)(0x0001u)
        #define sen14_INTR_FALLING   (uint16)(0x0002u)
        #define sen14_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen14_INTR_MASK      (0x01u) 
#endif /* (sen14__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen14_PS                     (* (reg8 *) sen14__PS)
/* Data Register */
#define sen14_DR                     (* (reg8 *) sen14__DR)
/* Port Number */
#define sen14_PRT_NUM                (* (reg8 *) sen14__PRT) 
/* Connect to Analog Globals */                                                  
#define sen14_AG                     (* (reg8 *) sen14__AG)                       
/* Analog MUX bux enable */
#define sen14_AMUX                   (* (reg8 *) sen14__AMUX) 
/* Bidirectional Enable */                                                        
#define sen14_BIE                    (* (reg8 *) sen14__BIE)
/* Bit-mask for Aliased Register Access */
#define sen14_BIT_MASK               (* (reg8 *) sen14__BIT_MASK)
/* Bypass Enable */
#define sen14_BYP                    (* (reg8 *) sen14__BYP)
/* Port wide control signals */                                                   
#define sen14_CTL                    (* (reg8 *) sen14__CTL)
/* Drive Modes */
#define sen14_DM0                    (* (reg8 *) sen14__DM0) 
#define sen14_DM1                    (* (reg8 *) sen14__DM1)
#define sen14_DM2                    (* (reg8 *) sen14__DM2) 
/* Input Buffer Disable Override */
#define sen14_INP_DIS                (* (reg8 *) sen14__INP_DIS)
/* LCD Common or Segment Drive */
#define sen14_LCD_COM_SEG            (* (reg8 *) sen14__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen14_LCD_EN                 (* (reg8 *) sen14__LCD_EN)
/* Slew Rate Control */
#define sen14_SLW                    (* (reg8 *) sen14__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen14_PRTDSI__CAPS_SEL       (* (reg8 *) sen14__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen14_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen14__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen14_PRTDSI__OE_SEL0        (* (reg8 *) sen14__PRTDSI__OE_SEL0) 
#define sen14_PRTDSI__OE_SEL1        (* (reg8 *) sen14__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen14_PRTDSI__OUT_SEL0       (* (reg8 *) sen14__PRTDSI__OUT_SEL0) 
#define sen14_PRTDSI__OUT_SEL1       (* (reg8 *) sen14__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen14_PRTDSI__SYNC_OUT       (* (reg8 *) sen14__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen14__SIO_CFG)
    #define sen14_SIO_HYST_EN        (* (reg8 *) sen14__SIO_HYST_EN)
    #define sen14_SIO_REG_HIFREQ     (* (reg8 *) sen14__SIO_REG_HIFREQ)
    #define sen14_SIO_CFG            (* (reg8 *) sen14__SIO_CFG)
    #define sen14_SIO_DIFF           (* (reg8 *) sen14__SIO_DIFF)
#endif /* (sen14__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen14__INTSTAT)
    #define sen14_INTSTAT            (* (reg8 *) sen14__INTSTAT)
    #define sen14_SNAP               (* (reg8 *) sen14__SNAP)
    
	#define sen14_0_INTTYPE_REG 		(* (reg8 *) sen14__0__INTTYPE)
#endif /* (sen14__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen14_H */


/* [] END OF FILE */
