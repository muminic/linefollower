/*******************************************************************************
* File Name: sen7.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sen7_H) /* Pins sen7_H */
#define CY_PINS_sen7_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sen7_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sen7__PORT == 15 && ((sen7__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    sen7_Write(uint8 value);
void    sen7_SetDriveMode(uint8 mode);
uint8   sen7_ReadDataReg(void);
uint8   sen7_Read(void);
void    sen7_SetInterruptMode(uint16 position, uint16 mode);
uint8   sen7_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the sen7_SetDriveMode() function.
     *  @{
     */
        #define sen7_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define sen7_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define sen7_DM_RES_UP          PIN_DM_RES_UP
        #define sen7_DM_RES_DWN         PIN_DM_RES_DWN
        #define sen7_DM_OD_LO           PIN_DM_OD_LO
        #define sen7_DM_OD_HI           PIN_DM_OD_HI
        #define sen7_DM_STRONG          PIN_DM_STRONG
        #define sen7_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define sen7_MASK               sen7__MASK
#define sen7_SHIFT              sen7__SHIFT
#define sen7_WIDTH              1u

/* Interrupt constants */
#if defined(sen7__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in sen7_SetInterruptMode() function.
     *  @{
     */
        #define sen7_INTR_NONE      (uint16)(0x0000u)
        #define sen7_INTR_RISING    (uint16)(0x0001u)
        #define sen7_INTR_FALLING   (uint16)(0x0002u)
        #define sen7_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define sen7_INTR_MASK      (0x01u) 
#endif /* (sen7__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sen7_PS                     (* (reg8 *) sen7__PS)
/* Data Register */
#define sen7_DR                     (* (reg8 *) sen7__DR)
/* Port Number */
#define sen7_PRT_NUM                (* (reg8 *) sen7__PRT) 
/* Connect to Analog Globals */                                                  
#define sen7_AG                     (* (reg8 *) sen7__AG)                       
/* Analog MUX bux enable */
#define sen7_AMUX                   (* (reg8 *) sen7__AMUX) 
/* Bidirectional Enable */                                                        
#define sen7_BIE                    (* (reg8 *) sen7__BIE)
/* Bit-mask for Aliased Register Access */
#define sen7_BIT_MASK               (* (reg8 *) sen7__BIT_MASK)
/* Bypass Enable */
#define sen7_BYP                    (* (reg8 *) sen7__BYP)
/* Port wide control signals */                                                   
#define sen7_CTL                    (* (reg8 *) sen7__CTL)
/* Drive Modes */
#define sen7_DM0                    (* (reg8 *) sen7__DM0) 
#define sen7_DM1                    (* (reg8 *) sen7__DM1)
#define sen7_DM2                    (* (reg8 *) sen7__DM2) 
/* Input Buffer Disable Override */
#define sen7_INP_DIS                (* (reg8 *) sen7__INP_DIS)
/* LCD Common or Segment Drive */
#define sen7_LCD_COM_SEG            (* (reg8 *) sen7__LCD_COM_SEG)
/* Enable Segment LCD */
#define sen7_LCD_EN                 (* (reg8 *) sen7__LCD_EN)
/* Slew Rate Control */
#define sen7_SLW                    (* (reg8 *) sen7__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sen7_PRTDSI__CAPS_SEL       (* (reg8 *) sen7__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sen7_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sen7__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sen7_PRTDSI__OE_SEL0        (* (reg8 *) sen7__PRTDSI__OE_SEL0) 
#define sen7_PRTDSI__OE_SEL1        (* (reg8 *) sen7__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sen7_PRTDSI__OUT_SEL0       (* (reg8 *) sen7__PRTDSI__OUT_SEL0) 
#define sen7_PRTDSI__OUT_SEL1       (* (reg8 *) sen7__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sen7_PRTDSI__SYNC_OUT       (* (reg8 *) sen7__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(sen7__SIO_CFG)
    #define sen7_SIO_HYST_EN        (* (reg8 *) sen7__SIO_HYST_EN)
    #define sen7_SIO_REG_HIFREQ     (* (reg8 *) sen7__SIO_REG_HIFREQ)
    #define sen7_SIO_CFG            (* (reg8 *) sen7__SIO_CFG)
    #define sen7_SIO_DIFF           (* (reg8 *) sen7__SIO_DIFF)
#endif /* (sen7__SIO_CFG) */

/* Interrupt Registers */
#if defined(sen7__INTSTAT)
    #define sen7_INTSTAT            (* (reg8 *) sen7__INTSTAT)
    #define sen7_SNAP               (* (reg8 *) sen7__SNAP)
    
	#define sen7_0_INTTYPE_REG 		(* (reg8 *) sen7__0__INTTYPE)
#endif /* (sen7__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sen7_H */


/* [] END OF FILE */
