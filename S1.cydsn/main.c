/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdlib.h>
#include "stdio.h"
#include "sensors.h"
#include "drive.h"
#include "console.h"
#include "esc.h"
#include "timer_time.h"
#include "bt_app.h"
#include "esc_control.h"
#include "utils.h"
#include "log2.h"


unsigned int sensorValues[10];
int32_t motorSpeed;
int Kp = 6;
int Kd = 40;//100
int baseSpeed = 50;
int turnSpeed = 50;
int lastError = 0; 
int errorbaseSpeed = 0;
int stop = 0;


void Go(long motorSpeed) // Braukšanas funkcija (motorSpeed ir PID izrēķinātā vērtība)
{
  if (motorSpeed >= 0)
  {
    if (motorSpeed <= errorbaseSpeed)
    {
      //leftm.drive(baseSpeed - motorSpeed);
      //rightm.drive(baseSpeed); 
        drive_motors(errorbaseSpeed - motorSpeed, errorbaseSpeed + motorSpeed);
    }
    else
    {
      //leftm.drive(-(motorSpeed - baseSpeed));
      //rightm.drive(baseSpeed); 
        drive_motors(-(motorSpeed - errorbaseSpeed), errorbaseSpeed + motorSpeed);
    }
  }

  if (motorSpeed < 0)
  {
    if (abs(motorSpeed) <= errorbaseSpeed)
    {
      //leftm(baseSpeed);
      //rightm(baseSpeed - abs(motorSpeed));
        drive_motors(errorbaseSpeed + abs(motorSpeed), errorbaseSpeed - abs(motorSpeed));
    }
    else
    {
      //leftm(baseSpeed);
      //rightm(-(abs(motorSpeed) - baseSpeed));
    drive_motors(errorbaseSpeed + abs(motorSpeed), -(abs(motorSpeed) - errorbaseSpeed));
    }
  }
}

int main(void)
{
    time_Start();
    log_setLevel(LOG_DEBUG);
    
    DEBUG_UART_Start();
    Motor_Init();
    Servo_Start();
    CyGlobalIntEnable; /* Enable global interrupts. */
    EscArm();
    //EscStop();
    //console_start();
    BT_Init();
    ADC_SAR_Start();
    ADC_SAR_StartConvert();
        
    //console_println("####LINEFOLLOWER####");
   ///console_print_int(microseconds());
    
    btValue.baseSpeed = baseSpeed;
    btValue.Kd = Kd;
    btValue.Kp = Kp;
    btValue.escSpeed = 1800 ;
    btValue.turnSpeed = turnSpeed;
    int oESC = 0;
    
    drive_forward(0,0);

    CyDelay(2000);
    log_info("T1 is ON!");
    
    for(;;)
    {
        if (Button_Read() == 0)
        {
            drive_forward(50,50);
        }
        else 
        {
            drive_forward(0,0);    
        }
            
        
        checkBtData();
        //sensorsRawConsole();
        if(btComands.escSpinning == 1)
        {
            escSpin();    
            
        }
        else 
        {
            EscStop();
            
        }
        
        if (btComands.driving == 1)
        {
            log_info("Start driving");
            while (btComands.driving == 1)
            {        
                checkBtStop();
                int position = checkSensors(5000,20); // speed 4 read per ms 
                log_debug("Position: %d", position);
        	    int error = (position - 7500); // 7500 ir vidējā vērtība
                
                if (abs(error) > 3500 )
                {
                    errorbaseSpeed = btValue.baseSpeed ;
                }
                else 
                {
                    errorbaseSpeed = btValue.baseSpeed +10 ;
                }
                
        	    motorSpeed = (btValue.Kp * error + btValue.Kd * (error - lastError)) / 2000; // / 1000 būtu pareizi...
        	    lastError = error;
                
                log_debug("Correction: %d", motorSpeed);
                
                if (position <= 0 )
                {
                    turn_right(btValue.turnSpeed, btValue.turnSpeed - 10);
                }
                else if (position >= 15000 )
                {
                    turn_left(btValue.turnSpeed - 10, btValue.turnSpeed);
                }
                else 
                {              
        	    Go(motorSpeed);               
                }
                
                //CyDelayUs(2560*2); 
            }
            EscStop();
            drive_backward(50,50);
            CyDelay(100);
            drive_forward(0,0);
            log_info("Robot Stopt");
        }
    }
}



/* [] END OF FILE */
