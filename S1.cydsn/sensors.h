/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef SENSORS_H
#define SENSORS_H

#include <project.h>
    
struct lineSenors
{
    int readValue[16];
    int calculatedValue[16];
    int thresholdValue[16];
    int detectedSensors[16];
    int readValuebuffer[50][16];
}gSensors;

void readSensors();
int checkSensors(int time, int count);

void calibrateSensors(int measures, int average);
void sensorsRaw(void);
void sensorsRawConsole(void);
void consolecheckSensors(void);

#endif /* SENSORS_H */