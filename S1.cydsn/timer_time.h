/* ========================================
 * Copyright (c) RobotuSkola, 2018-2019
 * Written by
 * - Einars Deksnis <einaarsd@gmail.com>, 2016
 * - Maris Becs <maris.becs@gmail.com>, 2016-2019
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute
 * this software is freely granted, provided that
 * this notice is preserved.
 * ========================================
*/
#ifndef TIMER_TIME_H
#define TIMER_TIME_H

#include <stdint.h>

//=============================================================================
// Public defines and typedefs
//=============================================================================
typedef struct {
	uint16_t sec;
	uint16_t ms;
	uint16_t ms_100th;
} psoc_time_t;

//=============================================================================
// Public function declarations
//=============================================================================
void time_Start(void);
void time_Reset(void);

uint32_t getMillis(void);
uint32_t getMicros(void);
psoc_time_t getTime(void);

#endif /* TIMER_TIME_H */
