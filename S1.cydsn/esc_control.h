/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef ESC_CONTROL_H
#define ESC_CONTROL_H

#include <project.h>


void EscArm(void);
void EscStop(void);
void EscSpeed(int outputESC);

void calib(void);
void escSpin();

#endif /* ESC_CONTROL_H */
/* [] END OF FILE */