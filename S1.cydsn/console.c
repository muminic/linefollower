/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "console.h"
#include <stdbool.h>
#include <string.h>
#include "timer_time.h"
#include <stdlib.h>
#include "stdio.h"

//=============================================================================
// Private defines and typedefs
//=============================================================================
#define USBUART_BUFFER_SIZE (64u)

const uint32_t TIMEOUT_MS = 100;   // 0.1 second

//=============================================================================
// Private function declarations
//=============================================================================
static void console_clear(void);
static void console_prompt(void);
static void console_process(const char str[]);
static bool console_isValidChar(const char c);

//=============================================================================
// Private data definitions
//=============================================================================
static bool console_enabled = false;

//=============================================================================
// Public function definitions
//=============================================================================
void console_start(void)
{
    USBUART_Start(0u, USBUART_3V_OPERATION);
    console_enabled = true;

    if (0 == USBUART_VBusPresent()) {
        //DLED3_Write(1);
        return; // TODO: USBUART_VBusPresent();
    }

    uint32_t start_time = millis();
    // Wait until component is ready to send data to host.
    while (true) {
        // Host can send double SET_INTERFACE request.
        if (0u != USBUART_IsConfigurationChanged()) {
            // Initialize IN endpoints when device is configured.
            if (0u != USBUART_GetConfiguration()) {
                // Enumeration is done, enable OUT endpoint for receive data from Host
                USBUART_CDC_Init();

                while (0 == USBUART_CDCIsReady()) {
                    if (500 < millis() - start_time) {
                        //DLED1_Write(1); // Indicate that USBUART is not working
                        return;
                    }
                }
                CyDelay(50);    // FIXME: Why??? Everything should work without this delay, but it does not!
                //console_println("Done!");
                //DLED2_Write(1);
                return;
            }
        }
        if (500 < millis() - start_time) {
            //DLED1_Write(1); // Indicate that USBUART is not working
            return;
        }
    };
}

void console_read()
{
    static char s_buffer[USBUART_BUFFER_SIZE + 1];
    static uint8 s_buffer_pos = 0;

    if (!console_enabled) return;
    if (0 == USBUART_VBusPresent()) return;

    /* Host can send double SET_INTERFACE request. */
    if (0u != USBUART_IsConfigurationChanged()) {
        /* Initialize IN endpoints when device is configured. */
        if (0u != USBUART_GetConfiguration()) {
            /* Enumeration is done, enable OUT endpoint for receive data from Host */
            USBUART_CDC_Init();
        }
    }

    if (!USBUART_DataIsReady()) return;

    unsigned char rec_data_buf[USBUART_BUFFER_SIZE + 1];
    USBUART_GetAll(rec_data_buf);
    uint8_t c = rec_data_buf[0]; // that's ok, if we lose rest of the data

    if (!console_isValidChar(c)) return;
    if (c != 127) { // DEL, hack for Tera Term...
        console_printChr(c);
    }

    if ('\r' == c) {
        console_printChr('\n');
        s_buffer[s_buffer_pos] = 0;
        console_process(s_buffer);
        s_buffer_pos = 0;
        return;
    }
    if (127 == c && 0 < s_buffer_pos) {
        // Backspace key pressed
        s_buffer_pos--;
        // VT100 commmands for Tera Term
        console_print("\033[D");    // Move the cursor backward by 1 column
        console_print("\033[K");    // Erase from the current cursor position to the end of the current line.
        return;
    }
    if (s_buffer_pos < USBUART_BUFFER_SIZE) {
        s_buffer[s_buffer_pos] = c;
        s_buffer_pos++;
    }
}

void console_disable(void)
{
    USBUART_Stop();
    console_enabled = false;
}

void console_print(const char str[])
{
    if (!console_enabled) return;
    if (0 == USBUART_VBusPresent()) return;

    uint32_t start_time = getMillis();
    /* Wait until component is ready to send data to host. */
    while (0u == USBUART_CDCIsReady()) {
        if (TIMEOUT_MS < getMillis() - start_time) return;
    };
    USBUART_PutString(str);
}

void console_println(const char line[])
{
    if (!console_enabled) return;
    if (0 == USBUART_VBusPresent()) return;

    console_print(line);
    console_print("\r\n");
}

void console_printChr(char c)
{
    if (0 == USBUART_VBusPresent()) return;

    uint32_t start_time = millis();
    while (0u == USBUART_CDCIsReady()) {
        if (TIMEOUT_MS < millis() - start_time) return;
    };
    USBUART_PutChar(c);
}

void console_data(const unsigned char data[], size_t len)
{
    if (0 == USBUART_VBusPresent()) return;

    uint32_t start_time = millis();
    while (0u == USBUART_CDCIsReady()) {
        if (TIMEOUT_MS < millis() - start_time) return;
    };
    USBUART_PutData(data, len);
}


// For GCC compiler revise _write() function for printf functionality
int _write(int file, char *ptr, int len) 
{
    (void)file;
    unsigned char buffer[len + 1];
    int i;
    for (i = 0; i < len; i++) 
    {
        buffer[i]= *ptr++;
    }
    console_data(buffer, len);

    return len;
}

//=============================================================================
// Private function definitions
//=============================================================================
void console_clear(void)
{
    if (!console_enabled) return;

    // VT100 commmand for Tera Term
    console_print("\033[2J");   // Erase the screen with the background colour and moves the cursor to home.
    console_print("\033[r");

    console_printChr('\f');
}

void console_prompt(void)
{
    if (!console_enabled) return;
    console_print("> ");
}

void console_process(const char str[])
{
    if (!console_enabled) return;

    char *delim = " ";
    size_t cmd_len = strcspn(str, delim);
    //size_t ws_len = strspn(str + cmd_len, delim);

    if (0 == str[0] || 0 == cmd_len) {
        // Do nothing
    }
    else if (strncmp("clear", str, cmd_len) == 0) {
        console_clear();
    }
    else {
        //test_process(str, cmd_len, str + cmd_len + ws_len);
    }

    console_prompt();
}

bool console_isValidChar(const char c)
{
    if (c == 9) return true;    // TAB
    if (c == 10) return true;   // LF
    if (c == 13) return true;   // CR
    if (c == 32) return true;   // Space
    if (c == 127) return true;  // DEL
    if (48 <= c && c <= 57) return true;    // 0-9
    if (65 <= c && c <= 90) return true;    // A-Z
    if (97 <= c && c <= 122) return true;   // a-z
    return false;
}

void console_print_int(int32_t number){
    char text[5];
    itoa(number, text, 10); 
    console_print(text);
}

/* [] END OF FILE */