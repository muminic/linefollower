/* ========================================
 * Copyright (c) RobotuSkola, 2018-2019
 * Written by
 * - Einars Deksnis <einaarsd@gmail.com>, 2016
 * - Maris Becs <maris.becs@gmail.com>, 2016-2019
 * All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute
 * this software is freely granted, provided that
 * this notice is preserved.
 * ========================================
*/
#include <project.h>
#include "timer_time.h"
#include "log2.h"

//=============================================================================
// Private defines and typedefs
//=============================================================================
#define TIMER_TC_VALUE      (50000)   // 0.5 sec at 100 kHz clock

const uint32_t SECOND = 1000u;

//=============================================================================
// Private function declarations
//=============================================================================
static uint16_t getMillis100th(void);
CY_ISR_PROTO(timer_tc_handler);

//=============================================================================
// Private data definitions
//=============================================================================
volatile static uint64_t s_roughMillis100th = 0u; // +/- 500 ms
volatile static uint32_t s_roughMicros = 0u;

//=============================================================================
// Public function definitions
//=============================================================================
void time_Start(void)
{
    TIMER_MS_WritePeriod(TIMER_TC_VALUE);
    TIMER_MS_WriteCounter(TIMER_TC_VALUE);
    TIMER_MS_Start();
    isr_500ms_StartEx(timer_tc_handler);
    while (0 == TIMER_MS_ReadCounter() && TIMER_TC_VALUE <= TIMER_MS_ReadCounter()) {
        // wait an instant till timer starts counting
    };
}

void time_Reset(void)
{
    const uint16_t counter = TIMER_MS_ReadCounter();
    s_roughMillis100th = -(TIMER_TC_VALUE - counter);
    s_roughMicros = -(TIMER_TC_VALUE - counter);

    log_bt("@");  // log fight start time in rs_logger
}

uint32_t getMillis(void)
{
    // NB: Timer actually counts down from TIMER_TC_VALUE to zero
    return ((s_roughMillis100th + (TIMER_TC_VALUE - TIMER_MS_ReadCounter())) / 100);
}

uint16_t getMillis100th(void)
{
    // NB: Timer actually counts down from TIMER_TC_VALUE to zero
    return ((s_roughMillis100th + (TIMER_TC_VALUE - TIMER_MS_ReadCounter())) % 100);
}

// NB: Will overflow after approximately 70 minutes!
//     Resolution is 10 microseconds!
uint32_t getMicros(void)
{
    return ((s_roughMicros + (TIMER_TC_VALUE - TIMER_MS_ReadCounter())) * 10);
}

psoc_time_t getTime(void)
{
    static psoc_time_t time = {0u, 0u, 0u};

    const uint32_t millis = getMillis();
	time.sec = millis / SECOND;
	time.ms = millis % SECOND;
    time.ms_100th = getMillis100th();

    return time;
}

//=============================================================================
// Private function definitions
//=============================================================================
CY_ISR(timer_tc_handler)
{
    s_roughMillis100th += TIMER_TC_VALUE;
    s_roughMicros += TIMER_TC_VALUE;
    TIMER_MS_ReadStatusRegister();
    isr_500ms_ClearPending();
}

/* [] END OF FILE */
