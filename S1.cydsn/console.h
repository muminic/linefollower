/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdio.h>

//=============================================================================
// Public function declarations
//=============================================================================
    
void console_start(void);
void console_disable(void);
void console_read(void);
void console_print(const char str[]);
void console_println(const char line[]);
void console_printChr(char c);
void console_data(const unsigned char data[], size_t len);
void console_print_int(int32_t number);

#endif /* CONSOLE_H */