/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef BT_APP_H
#define BT_APP_H

struct btComands 
{
    int driving;
    int escSpinning;
} btComands;

struct btValue 
{
    int Kp;
    int Kd;
    int Dt;
    int baseSpeed;
    int turnSpeed;
    int escSpeed;    
} btValue;
    
void checkBtData();
void BT_Init();
void checkBtStop();

#endif

/* [] END OF FILE */