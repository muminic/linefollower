/* ========================================
 *
 * Copyright RobotuSkola, 2018-2019
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ROBOTU SKOLA.
 *
 * ========================================
*/
#ifndef LOG_H
#define LOG_H

#include <stdio.h>

//=============================================================================
// Public defines and typedefs
//=============================================================================
enum {
    LOG_DEBUG,
    LOG_INFO,
    LOG_BLUETOOTH,
    LOG_WARN,
    LOG_ERROR,
};

#define log_debug(...) log_log(LOG_DEBUG, __VA_ARGS__)
#define log_info(...)  log_log(LOG_INFO,  __VA_ARGS__)
#define log_bt(...)  log_log(LOG_BLUETOOTH,  __VA_ARGS__)
#define log_warn(...)  log_log(LOG_WARN,  __VA_ARGS__)
#define log_error(...) log_log(LOG_ERROR, __VA_ARGS__)

//=============================================================================
// Public function declarations
//=============================================================================
void log_setLevel(int level);
void log_log(int level, const char *fmt, ...);

#endif /* LOG_H */
