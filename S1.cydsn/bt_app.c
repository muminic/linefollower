/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "bt_app.h"
#include "console.h"


void BT_Init()
{
    UART_BT_Start(); 
    UART_BT_ClearTxBuffer();
}

void checkBtData()
{
    //while(1)
    //{
        if (UART_BT_GetChar() == 0xAA)
        {

            int comand = 0 ; 
            while (comand == 0)
            {
                comand = (int)UART_BT_GetChar();
                if (comand > 0)
                {
                    break;    
                }
                console_println("1");
            }
            int comand2 = 0 ; 
            while (comand2 == 0)
            {
                comand2 = (int)UART_BT_GetChar();
                if (comand2 > 0)
                {
                    break;    
                }
                console_println("2");
            }
            int comand3 = 0 ; 
            while (comand3 == 0)
            {
                comand3 = (int)UART_BT_GetChar();
               if (comand3 > 0)
               {
                   break;    
               }
               console_println("3");
            }
            int comand4 = 0 ; 
            while (comand4 == 0)
            {
                comand4 = (int)UART_BT_GetChar();
               if (comand4 > 0)
               {
                   break;    
               }
               console_println("4");
            }
            int comand5 = 0 ; 
            while (comand5 == 0)
            {
                comand5 = (int)UART_BT_GetChar();
               if (comand5 > 0)
               {
                   break;    
               }
               console_println("5");
            }
            int comand6 = 0 ; 
            while (comand5 == 0)
            {
                comand6 = (int)UART_BT_GetChar();
               if (comand6 > 0)
               {
                   break;    
               }
               console_println("6");
            }
          
            console_print_int(comand);
            console_print_int(comand2);
            console_print_int(comand3);
            console_print_int(comand4);
            console_print_int(comand5);
            console_print_int(comand6);

            if (comand == 0xA2)
            {
                btComands.escSpinning = comand2;  
                btValue.escSpeed = 1000 + (10*comand3);
                console_print("esc comand");        
            }
            if (comand == 0xA1)
            {
                btComands.driving = comand2;  //ON/OFF driving
                console_print("motor comand");
            }
            if (comand == 0xA4)
            {
                UART_BT_PutChar(0xBB);
                UART_BT_PutChar(0x01);
                UART_BT_PutChar(0x02);
                UART_BT_PutChar(0x03);
                
                console_print("read");
            }
            if (comand == 0xA6) //sen
            {
                btValue.Kp = comand2;
                btValue.Kd = comand3;
                btValue.Dt = comand4;
                btValue.baseSpeed = comand5;
                btValue.turnSpeed = comand6;
                
                console_print("pid constant");
            }
            if (comand == 0xA9) // read
            {

            }
            //console_print("AA");
    
        //console_println("W");
        UART_BT_ClearRxBuffer();    
        //break;
        }
        
    //}
}

void checkBtStop()
{
        if (UART_BT_GetChar() == 0xAA)
        {
            int comand = 0 ; 
            while (comand == 0)
            {
                comand = (int)UART_BT_GetChar();
                if (comand > 0)
                {
                    break;    
                }
                //console_println("1");
            }
            int comand2 = 0 ; 
            while (comand2 == 0)
            {
                comand2 = (int)UART_BT_GetChar();
                if (comand2 > 0)
                {
                    break;    
                }
                //console_println("2");
            }
          
            console_print_int(comand);
            console_print_int(comand2);

            if (comand == 0xA1)
            {
                btComands.driving = comand2;  //ON/OFF driving
                //console_print("motor comand");
            }
        UART_BT_ClearRxBuffer();    
        }
        
}






/* [] END OF FILE */
