-- ======================================================================
-- USBFS_Bootloader01.ctl generated from USBFS_Bootloader01
-- 01/11/2019 at 11:16
-- This file is auto generated. ANY EDITS YOU MAKE MAY BE LOST WHEN THIS FILE IS REGENERATED!!!
-- ======================================================================

-- TopDesign
-- =============================================================================
-- The following directives assign pins to the locations specific for the
-- CY8CKIT-050 PSoC5 kit.
-- =============================================================================

-- === USBFS ===
attribute port_location of \USBFS:Dp(0)\ : label is "PORT(15,6)";
attribute port_location of \USBFS:Dm(0)\ : label is "PORT(15,7)";


-- === LED4 ===
attribute port_location of LED4(0) : label is "PORT(6,3)";
-- PSoC Clock Editor
-- Directives Editor
-- Analog Device Editor
